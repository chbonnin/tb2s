if [ "$#" -lt 3 ]; then
	echo Usage: $0 \<Sensor\> \<Token>\> \<Datafile\(s\)\>
	echo Import Metatracker sensor data into InfluxDB time series database
	echo Datatype is deduced from file name\'s ending and can be one of: accelerometer, temperature, light, gyroscope, humidity, pressure
	exit 1
fi
SENSOR=$1
shift
TOKEN=$1
shift
while test $# -gt 0
do
  if [[ $1 == *Accelerometer*.csv ]]; then
		FIELD="x-axis|double,y-axis|double,z-axis|double"
  elif [[ $1 == *Gyroscope*.csv ]]; then
		FIELD="x-gyro|double,y-gyro|double,z-gyro|double"
  elif [[ $1 == *Light*.csv ]]; then
		FIELD="light|double"
  elif [[ $1 == *Humidity*.csv ]]; then
		FIELD="humidity|double"
  elif [[ $1 == *Pressure*.csv ]]; then
		FIELD="pressure|double"
  elif [[ $1 == *Temperature*.csv ]]; then
		FIELD="temperature|double"
  else
        echo Unknown data type for $1
        shift
        continue
  fi
  set -x
  influx write -t $TOKEN -b ladder -o IPHC --skipHeader --header '#constant measurement,measure' --header "#constant tag,sensor,$SENSOR" \
  --header '#timezone +0200' --header ",time|dateTime:2006-01-02T15.04.05.999,,$FIELD" --file "$1"
  set +x
  shift
done
