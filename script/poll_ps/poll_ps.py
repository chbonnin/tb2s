import subprocess
from datetime import datetime
from influxdb_client import InfluxDBClient, Point, WritePrecision
from influxdb_client.client.write_api import SYNCHRONOUS
import requests
import time
import sys
import re
from TCP_Interface import TCPClient
import argparse


parser = argparse.ArgumentParser(description='Send commands to PowerSupplyController')
parser.add_argument('-i','--psid', help = 'Power supply ID', default = 'CAEN-SY5527',type = str)
parser.add_argument('-p','--port', help = 'Port number of PowerSupplyController', default = 0,type = int)
parser.add_argument('-H','--host', help = 'Host of PowerSupplyController (ex.: 127.0.0.1)',default='127.0.0.1', type = str)
parser.add_argument('-c','--channel', help = 'Channel ID (ex.: LV1-0)',type = str)
parser.add_argument('-m','--command', help = 'command (ex.: SetVoltage,Voltage:50 or TurnOn or getCurrent or SetVProtection,VoltageProtection:100 or SetVCompliance,VoltageCompliance:100 ...)', type = str)
parser.add_argument('-r','--regExp', help = 'Regular expression to match channel id (with GetStatus)', type = str)
parser.add_argument('-d','--delay', help = 'Delay after each voltage setting', default = 0,type = int)
args = parser.parse_args()

def upload_timeseries(pBucket,channel, field, value):
    try:
        token = "Kl_kkEYaEQrJD5GHzxeUYKEv_VmTFDQtTuljzJmcKRhBHyMoSq92C-DOiBf_iadaMt3xMnhkptbzZ7CIUrvazg=="
        org = "IPHC"
        bucket = pBucket
        with InfluxDBClient(url="http://sbgpcs76:8086", token=token, org=org) as client:
            write_api = client.write_api(write_options=SYNCHRONOUS)
            point = Point("PowerSupply") \
            .tag("host", channel).tag("type",field) \
            .field("field", value) \
            .time(datetime.utcnow(), WritePrecision.NS)
            write_api.write(bucket, org, point)
        #print("Upload sccessfully")
    except requests.exceptions.Timeout as ex:
        self.log.warning("Time series upload timed out: %s", ex)
    except requests.exceptions.RequestException as ex:
        self.log.warning("Time series upload failed: %s", ex)
    except:
        pass

if __name__ == '__main__':
    #if len(sys.argv)<2:
        #print("Usage:", sys.argv[0], "<PowerSupplyID> <Controller port number> <command> <channel regular expression>")
        #sys.exit(1)

    ps = str(args.psid)
    
    print("power supply: " + str(ps))

    client = TCPClient(args.port, args.host)
    client.connectClient()
    if args.command: 
        strCmd = args.command+",PowerSupplyId:"+ps
        if args.channel:
            strCmd+=",ChannelId:"+args.channel
    else:
        strCmd= "GetStatus,PowerSupplyId:" + ps

    print("command:", strCmd)
    while True:
        #print(result[ps])
        tps, result = client.sendAndReceivePacket(strCmd)
        if ps in result:
          for ch in result[ps].keys():
            if not args.regExp or re.search(args.regExp, ch):
                print(ch,result[ps][ch]["Voltage"], "V,",result[ps][ch]["Current"], "A" if "LV" in ch else "µA")
                upload_timeseries("ladder",ps+"_"+ch, "voltage", float ( result[ps][ch]["Voltage"] ) )
                upload_timeseries("ladder",ps+"_"+ch, "current", float ( result[ps][ch]["Current"] ) )
        else:
            print(result)
        
        if args.delay>0:
            time.sleep(args.delay)
        else:
            break

