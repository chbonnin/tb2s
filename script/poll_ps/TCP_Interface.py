from PySide2.QtCore import Signal, QObject
import os, subprocess, time, datetime, socket, ctypes
import sys

BUFFER_SIZE = 65535


class TCP_Interface(QObject):
    update = Signal ( object , object )

    def __init__( self,  pIndex, pPackageFolder, pConfigFile):
        super(TCP_Interface, self).__init__(  )
        print("GIPHT:\tNew TCP Interface")
        self.server = TCPServer(pIndex, pPackageFolder)
        self.server.start(pConfigFile)
        time.sleep(0.5)
        self.client = TCPClient(pIndex)
        self.client.connectClient()
        self.client.tcpAnswer.connect(self.handleAnswer)

    def executeCommand( self , pCmd, pPrintLog = False):
        self.client.sendAndReceivePacket(pCmd)

    def handleAnswer( self, pAnswer ):
        if pAnswer is not None:
            self.update.emit("data", pAnswer)
        else:
            self.update.emit("noData", None)

    #To be defined    
    def stopTask( self ):
        pass



class TCPServer():
    def __init__( self, pIndex, pFolder = None ):
        print("GIPHT:\tNew TCP Server")
        self.index = pIndex
        self.folder = pFolder
        self.config = None
        self.serverProcess = None
        self.logFile = open("logs/log" + str(self.index) + ".txt", "w+")

    def start( self , pConfigFile ) :
        self.config = pConfigFile
        if self.folder is not None:
            print("GIPHT:\tTCP Server: Start TCP Device Server at " + self.folder + " with config file " + self.config)
            if not self.isRunning():
                cmd = "source setup.sh && ./bin/PowerSupplyController -c " + self.config + " -p " + str( 7010 + self.index)
                self.serverProcess = subprocess.Popen(cmd, stdout=self.logFile, stderr=subprocess.PIPE, encoding = 'UTF-8', shell= True, executable="/bin/bash", cwd=self.folder)
                #self.serverProcess = subprocess.Popen(cmd, shell=True,executable="/bin/bash", cwd="/home/readout/Stefan/GUI/gipht/power_supply/")
                return True
            else:
                print("GIPHT:\tTCP Device Server already running")
                return False
        else:
            print("GIPHT:\tNo folder given for TCP Device Server, abort!")
            return False

    def checkDeviceSubProcess ( self ):
        search = subprocess.Popen(['ps', '-A'], stdout=subprocess.PIPE)
        output, error = search.communicate()
        target_process = "PowerSupplyCont"

        for line in output.splitlines():
            #print(str(line))
            if target_process in str(line):
                pid = int(line.split(None, 1)[0])
                #print("PID:" + str(pid))


        if self.serverProcess is not None:
            response = self.serverProcess.poll()
            print( "Response: " + str( response) )
            print("GIPHT:\tCheck subprocess of device " + self.config + ": is running")
            return  response is None
        else:
            print("GIPHT:\tCheck subprocess of device " + self.config + ": is not running")
            return False   

    def killAnyPowerSupplyProcess( self ):
        search = subprocess.Popen(['ps', '-A'], stdout=subprocess.PIPE)
        output, error = search.communicate()
        target_process = "PowerSupplyCont"

        for line in output.splitlines():
            if target_process in str(line):
                pid = int(line.split(None, 1)[0])

                os.kill(pid, 9)
                return True
        return False
    def stop ( self ):
        print("TCP Server: Kill TCP Device Server at " + self.folder + " with config file " + self.config)

        return self.killAnyPowerSupplyProcess()

    def isRunning( self):
        return self.checkDeviceSubProcess()


class TCPClient(QObject):
    tcpAnswer = Signal( object )

    def __init__( self, pIndex, host="127.0.0.1"):
        super(TCPClient, self).__init__(  )
        self.index = pIndex
        self.host=host
        print("GIPHT:\tNew TCP Client " + str( self.index ))
        
        self.fPackerNumber = 0
        self.fClientSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.lastPoll = datetime.datetime.now()
        self.lastData = None
        self.running = False

    def __del__(self):
        self.running = False
        self.fClientSocket.close()

    def isRunning( self ):
        return self.running

    def connectClient ( self ):
        #self.client.connect(2,1000)
        try:
            self.fClientSocket.connect((self.host, 7010 + self.index))
            self.running = True
        except socket.error as e:
            #print(e)
            self.running = False
        
    def disconnectClient( self ):
        self.fClientSocket.close()

    def makeFourByteArray(self, integer):
        integerByte0 = ctypes.c_ubyte((integer >>  0) & 0xff)
        integerByte1 = ctypes.c_ubyte((integer >>  8) & 0xff)
        integerByte2 = ctypes.c_ubyte((integer >> 16) & 0xff)
        integerByte3 = ctypes.c_ubyte((integer >> 24) & 0xff)
        integerByte = bytearray(integerByte3) + bytearray(integerByte2) + bytearray(integerByte1) + bytearray(integerByte0)
        return integerByte

    def encodePacket(self, message, packetNumber):
        fullMessage = self.makeFourByteArray(len(message)+8) + self.makeFourByteArray(packetNumber) + bytearray(message, "utf-8")
        return fullMessage;
    
    def decodePacket(self, message):
        return message[8:].decode();

    def sendAndReceivePacket(self, message):
        if self.running:
            #print(str(self.index) + "\t" +message)
            try:
                self.fClientSocket.send(self.encodePacket(message, self.fPackerNumber))
                self.fPackerNumber = self.fPackerNumber +1
                data = self.fClientSocket.recv(BUFFER_SIZE)
                result = self.decodeStatus(data)
                #print("res=",result)
                self.tcpAnswer.emit(result)
                return result
            except BrokenPipeError:
                print("TCP Broken Pipe error!", file=sys.stderr)

        else:
            print("TCP Socket not connected!", file=sys.stderr)

    def decodeStatus( self, pStatus ):
        strDecoded=self.decodePacket(pStatus)
        #print("decoded=",strDecoded)
        statusList = strDecoded.split(',')

        timestamp = None
        statusDict = {}


        splitTimestamp = statusList[0].split(':')
        if len(splitTimestamp)<=3:
            return None, strDecoded

        timestamp = splitTimestamp[1] + ":" + splitTimestamp[2] + ":" + splitTimestamp[3]

        #for entry in statusList[1:]: device = entry.split('_')[0]

        ###for entry in statusList[1:]:
            ###device = entry.split('_')[0]
            ###channel = entry.split('_')[1]

        for entry in statusList[1:]:
            arrDev=entry.split('_')
            if len(arrDev)>3:
                print("Forbidden char '_' present in PowerSupply ID or in channel ID", file=sys.stderr)
                break

            device = arrDev[0]
            channel = arrDev[1]
            meas, value = arrDev[2].split(':')
            if not device in statusDict: 
                statusDict[device] = {}
            if not channel in statusDict[device]:
                statusDict[device][channel] = {}

            statusDict[device][channel][meas]=value

        return timestamp, statusDict
