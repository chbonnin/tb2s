import requests
import time
import sys
from TCP_Interface import TCPClient
import argparse
import matplotlib.pyplot as plt


parser = argparse.ArgumentParser(description='Plot of IV curve data using matplotlib ')
parser.add_argument('-i','--psid', help = 'Power supply ID', default = 'CAEN-SY5527',type = str)
parser.add_argument('-p','--port', help = 'Port number of PowerSupplyController', default = 0,type = int)
parser.add_argument('-c','--channel', help = 'Channel ID', default = 'LV1-0',type = str)
parser.add_argument('-f','--filename', help = 'Output CSV filename prefix', default = 'data/iv',type = str)
parser.add_argument('-r','--read', help = 'read data from CSV file', type = str)
parser.add_argument('-w','--showPlot', help = 'shows plot', action = 'store_true')
parser.add_argument('-l','--lowVoltage', help = 'Low voltage', action = 'store_true')
parser.add_argument('-b','--begin', help = 'Beginning of voltage range', default = 0,type = float)
parser.add_argument('-e','--end', help = 'End of voltage range', default = 10,type = float)
parser.add_argument('-s','--step', help = 'Step of voltage range', default = 1,type = float)
parser.add_argument('-d','--delay', help = 'Delay after each voltage setting', default = 1,type = int)
args = parser.parse_args()

def showFile(filename):
    #print("show file:", filename)
    with open(filename, 'r') as filShow:
        arrLines = filShow.readlines()

    arrX=[]
    arrY=[]
    for strLine in arrLines:
        arrSplit = strLine.split(",")
        if len(arrSplit)>1: 
            arrX.append(float(arrSplit[0]))
            arrY.append(float(arrSplit[1]))

    plt.plot(arrX, arrY, 'bo-') #color="blue", marker="o", linestyle="solid"
    plt.plot([0,800],[0,8],'r-')
    plt.ylabel("A" if args.lowVoltage else "µA")
    plt.xlabel("V")
    plt.title("IV on "+args.channel)
    plt.show()
        
if __name__ == "__main__":
    client = TCPClient(args.port)
    client.connectClient()

    if args.read:
        showFile(args.read)
        sys.exit(0)

    if not args.lowVoltage:
        print(client.sendAndReceivePacket("TurnOn,PowerSupplyId:"+args.psid+",ChannelId:"+args.channel))
        time.sleep(args.delay)

    outFile=open(args.filename+args.channel+".csv", "w")
    val=args.begin
    while args.end >= val:
        client.sendAndReceivePacket("SetVoltage,PowerSupplyId:"+args.psid+",ChannelId:"+args.channel+",Voltage:"+str(val))
        time.sleep(args.delay)
        if args.lowVoltage:
            print(client.sendAndReceivePacket("TurnOn,PowerSupplyId:"+args.psid+",ChannelId:"+args.channel))

        time.sleep(1)
        _ , curr=client.sendAndReceivePacket("GetCurrent,PowerSupplyId:"+args.psid+",ChannelId:"+args.channel)
        time.sleep(1)
        _ , volt=client.sendAndReceivePacket("GetVoltage,PowerSupplyId:"+args.psid+",ChannelId:"+args.channel)
        print(volt,"V", curr,"A" if args.lowVoltage else "µA")
        print(volt+","+curr, file=outFile)
        if args.lowVoltage:
            print(client.sendAndReceivePacket("TurnOff,PowerSupplyId:"+args.psid+",ChannelId:"+args.channel))

        time.sleep(1) 
        val+=args.step

    outFile.close()
    if not args.lowVoltage:
        print(client.sendAndReceivePacket("TurnOff,PowerSupplyId:"+args.psid+",ChannelId:"+args.channel))

    if args.showPlot:
        showFile(args.filename+args.channel+".csv")
