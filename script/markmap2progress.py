#!/usr/bin/python3
# coding: utf-8
import sys
import re

if len(sys.argv)<2:
    print("Transform markdown file titles into an HTML nested list with color and collapsable tree items")
    print("Count duration in Days, colors items in green if line ends with '.'")
    print("Usage",sys.argv[0]," <file.md>")
    sys.exit(1)

with open (sys.argv[1]) as fName:
    arrLines=fName.readlines()

print("""
<html>
<head>
<META http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
<style>
/* fonctionnel */
input {display: none;}
input ~ ul {display: none;}
input:checked ~ ul {display: block;}
input ~ .fa-angle-down {display: none;}
input:checked ~ .fa-angle-right {display: none;}
input:checked ~ .fa-angle-down {display: inline;}
/* habillage */
li {
  display: block;
  font-family: 'Arial';
  font-size: 12px;
  padding: 0.2em;
  border: 1px solid transparent;
}
li:hover {
  border: 1px solid grey;
  border-radius: 3px;
  /*background-color: lightgrey;*/
}
.ok {background-color: lime;}
.ko {background-color: red;}
.orange {background-color: orange;} 
</style>
</head>
<body>
<ul>
""")

nb= nbPrev=0
nbDur=0
iId=1
arrLines.append("")
for line in arrLines:
    if not line.startswith("#"):
        continue

    line=line.strip()
    nbPrev=nb
    mLevel=re.match(r"(#+) ", line) 

    nb=len(mLevel.group(1)) if mLevel else 0
    if nbPrev>0 :
        mDur=re.search(r" (\d+)[dD]$", strPrev)
        mDone=re.search(r"\.$", strPrev)
        strClass = ("ok" if mDone else "ko")
        if mDur:
            nbDur += int(mDur.group(1))
            if mDur.group(0).endswith("d"):
                strClass="orange"

        strItem= strPrev[nbPrev+1 : mDur.start(1) if mDur else len(strPrev)]
        if nb>nbPrev:# up ^
            print(" "*(2*nbPrev) + "<li class='"+strClass+"'>")
            print(" "*(2*nbPrev+1) + "<input type='checkbox' checked id='id{0}'><label for='id{0}'>".format(iId)+strItem+"</label> <i class='fa fa-angle-right'></i> <i class='fa fa-angle-down'></i>")
            iId+=1
            for iLev in range(nbPrev, nb):
                print(" "*(2*iLev+1) + "<ul>")

        elif nb<nbPrev:# down v
            print(" "*(2*nbPrev) + "<li class='"+strClass+"'>"+strItem+"</li>")
            for iLev in range(nbPrev, nb, -1):
                print(" "*(2*iLev-1) + "</ul>")

            print(" "*(2*nb) + "</li>")
        else:# equal >
            print(" "*(2*nb) + "<li class='"+strClass+"'>"+strItem+"</li>")

    strPrev=line

print("</ul>\nDuration: {0}\n </body> </html> ".format(nbDur))
