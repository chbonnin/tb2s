#!/usr/bin/python3
# coding: utf-8
import sys
import http.client
import optparse 
import influxdb

parser = optparse.OptionParser(usage = "%prog [options] <import csv file> \n\n"
   "Importation of CSV data into InfluxDB database"
)
parser.add_option("-l", "--line", action="store", dest="line", help="Choose number of line to process (starts with 2, 0 means all)", default="0")

(options, args) = parser.parse_args()

if len(args)<1: 
	parser.print_help()
	sys.exit()

dbCli= influxdb.InfluxDBClient('localhost')
dbCli.switch_database("integration")
arrPoints=[]
numLine=1
numExpect=int(options.line)
with open (args[0]) as fName:
    arrLines=fName.readlines()[1:]#skip header

for line in arrLines:
    numLine+=1
    if numExpect==0 or numExpect==numLine:
        print (numLine, line)
        arrFld=line.split(',')
        point={"measurement": "sensor", 
        "tags":{"location": "metatracker", "measurement": "lumi"}, 
        "fields":{"value":float(arrFld[-1])}, # illuminance field
        "time":int(arrFld[0]) #epoch field
        }
        arrPoints.append(point)

print("nb of points:",len(arrPoints))
print("Result:",dbCli.write_points(arrPoints, time_precision='ms'))
    #print(arrFld[0], float(arrFld[-1]))


