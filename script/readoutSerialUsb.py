#!/usr/bin/python3
# coding: utf-8
import serial
import argparse 
import time

parser = argparse.ArgumentParser(description="Readout Arduino output via USB")
parser.add_argument("device", help="USB device")
parser.add_argument("-d", "--delay", help="Repeat delay in seconds (0 means no repeat)", type=int, default=1)
parser.add_argument("-t", "--timeout", help="Serial communication timeout", type=float, default=0.1)
parser.add_argument("-s", "--speed", help="Serial communication speed", type=int, default=115200)
args = parser.parse_args()

arduino = serial.Serial(args.device, args.speed, timeout=args.timeout)

while True:
    strOut = arduino.readline().decode("utf8").strip()
    if strOut:
        print(strOut)

    if args.delay:
        time.sleep(args.delay)
    else:
        break

arduino.close()

