#!/bin/bash
# description
printf "**** Downgrading wpasupplicant for Eduroam use ****\n"

echo Today is
date
read -p "Date must be correct before going futher, is it ok ?" -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[YyOo]$ ]]
then
    # check connection before going further
    wget -q --spider http://google.com
    if [ $? -eq 0 ]; then
        # proceed
        sudo apt-get remove wpasupplicant -y
        sudo mv -f /etc/apt/sources.list /etc/apt/sources.list.bak
        sudo bash -c "echo 'deb http://raspbian.raspberrypi.org/raspbian/ stretch main contrib non-free rpi' > /etc/apt/sources.list"
        sudo apt-get update
        sudo apt-get install wpasupplicant -y
        sudo apt-mark hold wpasupplicant
        sudo cp -f /etc/apt/sources.list.bak /etc/apt/sources.list
        sudo apt-get update
    else
        echo "no internet, check your connection"
    fi
else
    echo "use the command sudo date --utc -s """25 OCT 2018 10:47:00""" "
fi
