#!/usr/bin/python3
# coding: utf-8
import socket
import optparse 
import time
import datetime
import requests
from influxdb_client import InfluxDBClient, Point, WritePrecision
from influxdb_client.client.write_api import SYNCHRONOUS

parser = optparse.OptionParser(usage = "%prog [options] \n\n"
   "Sends socket request to power supply device to retrieve the measured current and store it to Influx DB"
)
parser.add_option("-i", "--ip", action="store", dest="ip", help="Power supply host", default="sbgps13")
parser.add_option("-p", "--port", action="store", dest="port", type="int", help="Power supply port", default="10001")
parser.add_option("-c", "--channel", action="store", dest="channel", help="Channel number", default="0")
parser.add_option("-d", "--delay", action="store", dest="delay", type="float", help="Repeat delay between two measures (0 means no repeat)", default="0")
parser.add_option("-b", "--bucket", action="store", dest="bucket", help="InfluxDB bucket name ('-' means no DB)", default="ladder")
parser.add_option("-s", "--silent", action="store_true", dest="silent", help="Don't print measures", default=False)
(options, args) = parser.parse_args()

def upload_timeseries(pBucket, host, field, value):
    try:
        token = "Kl_kkEYaEQrJD5GHzxeUYKEv_VmTFDQtTuljzJmcKRhBHyMoSq92C-DOiBf_iadaMt3xMnhkptbzZ7CIUrvazg=="
        org = "IPHC"
        bucket = pBucket
        with InfluxDBClient(url="http://sbgpcs76:8086", token=token, org=org) as client:
            write_api = client.write_api(write_options=SYNCHRONOUS)
            point = Point("PowerSupply") \
            .tag("host", host).tag("type",field) \
            .field("value", value) \
            .time(datetime.datetime.utcnow(), WritePrecision.NS)
            write_api.write(bucket, org, point)
        # print("Upload successfull")
    except requests.exceptions.Timeout as ex:
        self.log.warning("Time series upload timed out: %s", ex)
    except requests.exceptions.RequestException as ex:
        self.log.warning("Time series upload failed: %s", ex)
    except Exception as err:
        print("Exception:", err)
        pass

if __name__ == '__main__':
    device = socket.socket()
    device.connect((options.ip, options.port))
    while True:
        strCmd = f":MEAS:CURR? (@{options.channel})\r\n"
        device.send(strCmd.encode())
        fMeasure = float(device.recv(256).decode().strip()[:-1])
        if options.bucket != '-':
            upload_timeseries(options.bucket, options.ip+"_"+options.channel, "current", fMeasure)

        if not options.silent:
            print(fMeasure)

        if options.delay > 0:
            time.sleep(options.delay)
        else:
            break

    device.close()

