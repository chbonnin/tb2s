// SHT85
#include "SHT85.h"
#define SHT85_ADDRESS         0x44
SHT85 sht;

// SHT31
// Problem : même adresse: 0x44 !!
//#include <DFRobot_SHT3x.h>
//DFRobot_SHT3x sht3x(&Wire, 0x44);

// DHT12
#include <DHT.h>
#define DHTPIN 4
DHT dht(DHTPIN, DHT22);

// PT100 RTD Sensor w/MAX31865
#include <Adafruit_MAX31865.h>
// Use software SPI: CS, DI, DO, CLK
Adafruit_MAX31865 pt100 = Adafruit_MAX31865(10, 12, 11, 13);
// The value of the Rref resistor. Use 430.0 for PT100 and 4300.0 for PT1000
#define RREF      430.0
// The 'nominal' 0-degrees-C resistance of the sensor
// 100.0 for PT100, 1000.0 for PT1000
#define RNOMINAL  100.0

// MLX90614 InfraRed Temp Sensor
#include <Adafruit_MLX90614.h>
Adafruit_MLX90614 mlx = Adafruit_MLX90614();


// NTC probes
#define B_103JT 3435 // ntc b constant
#define B_103F 3984 // ntc b constant
#define B_540G 3612 // ntc b constant
#define RESISTOR 20000 // résistance de la résistance, 20 kOhm
#define THERMISTOR 10000 // résistance nominale de la thermistor, 10 kOhm
#define NOMINAL 25 // température nominale
#define ntc0 A0
#define ntc1 A1
#define ntc2 A2
#define ntc3 A3


void Temp_from_NTC_R(uint8_t adcPin, uint16_t bCoef) {
    bool debug=false;

    int t = 1023 - analogRead(adcPin); // mesure ADC faite sur la résistance nominale -> soustraction
    if(debug){
      Serial.print("adc=");
      Serial.print(t);
    }
    float tr = 1023.0 / t - 1;
    tr = RESISTOR / tr ;
    if(debug){
      Serial.print(" R=");
      Serial.print(tr);
      Serial.print(", T=");
    }

    float steinhart;
    steinhart = tr / THERMISTOR;
    steinhart = log(steinhart);
    steinhart /= bCoef;
    steinhart += 1.0 / (NOMINAL + 273.15);
    steinhart = 1.0 / steinhart;
    steinhart -= 273.15; 
    Serial.print(steinhart);
}

int timeStep = 5; //seconds

void setup() {
  // put your setup code here, to run once:
 
  // Open serial communications and wait for port to open:
  Serial.begin(115200);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }

  Wire.begin();
  sht.begin(SHT85_ADDRESS);
  Wire.setClock(100000);

  Serial.println("Setting up SHT85 module:");
  uint16_t stat = sht.readStatus();
  Serial.print("Status: ");
  Serial.print(stat, HEX);
  Serial.print(" ");

  uint32_t ser = sht.GetSerialNumber();
   Serial.print("Serial Number: ");
  Serial.print(ser, HEX);
  Serial.println("");
  delay(1000);

  /*Serial.println("Setting up SHT31 module:");
  while (sht3x.begin() != 0) {
    Serial.println("Failed to Initialize the chip, please confirm the wire connection");
    delay(1000);
  }
  Serial.print("Chip serial number: ");
  Serial.println(sht3x.readSerialNumber());
  if(!sht3x.softReset()){
     Serial.println("Failed to Initialize the chip....");
  }*/

  pinMode(DHTPIN, INPUT);
  Serial.println("Setting up DHT22 sensor");
  dht.begin();

  Serial.println("Setting up MAX31865 PT100 sensor");
  pt100.begin(MAX31865_4WIRE);  // set to 2WIRE or 4WIRE as necessary

  Serial.println("Setting up  MLX90614 IR sensor");
  if (!mlx.begin()) {
    Serial.println("Error connecting to MLX sensor. Check wiring.");
    //while (1);
  };
  Serial.print("Emissivity = "); Serial.println(mlx.readEmissivity());

  // print header
  Serial.println("");
  Serial.println("================================================");
  Serial.print("T_sht85");
  Serial.print("\t");
  Serial.print("H_sht85");
  Serial.print("\t");
  //Serial.print("T_sht31");
  //Serial.print("\t");
  //Serial.print("H_sht31");
  Serial.print("T_dht22");
  Serial.print("\t");
  Serial.print("H_dht22");
  Serial.print("\t");
  Serial.print("T_pt100");
  Serial.print("\t");
  Serial.print("T_ir");
  Serial.print("\t");
  Serial.print("T_ntc_0");
  Serial.print("\t");
  Serial.print("T_ntc_1");
  Serial.print("\t");
  Serial.print("T_ntc_2");
  Serial.print("\t");
  Serial.print("T_ntc_3");
  Serial.println("");

}

void loop() {
  // put your main code here, to run repeatedly:
 
  sht.read();
  Serial.print(sht.getTemperature(), 1);
  Serial.print("\t");
  Serial.print(sht.getHumidity(), 1);
  Serial.print("\t");

  //Serial.print(sht3x.getTemperatureC(), 1);
  //Serial.print("\t");
  //Serial.print(sht3x.getHumidityRH());
  //Serial.print("\t");

  Serial.print(dht.readTemperature(), 1);
  Serial.print("\t");
  Serial.print(dht.readHumidity(), 1);
  Serial.print("\t");

  Serial.print( pt100.temperature(RNOMINAL, RREF), 1);
  Serial.print("\t");
  
  /*Serial.println("");
  uint16_t rtd = pt100.readRTD();
  Serial.print("RTD value: "); Serial.println(rtd);
  float ratio = rtd;
  ratio /= 32768;
  Serial.print("Ratio = "); Serial.println(ratio,8);
  Serial.print("Resistance = "); Serial.println(RREF*ratio,8);
  Serial.print("Temperature = "); Serial.println(pt100.temperature(RNOMINAL, RREF));*/

// Check and print any faults
  uint8_t fault = pt100.readFault();
  if (fault) {
    Serial.println("");
    Serial.print("Fault 0x"); Serial.println(fault, HEX);
    if (fault & MAX31865_FAULT_HIGHTHRESH) {
      Serial.println("RTD High Threshold"); 
    }
    if (fault & MAX31865_FAULT_LOWTHRESH) {
      Serial.println("RTD Low Threshold"); 
    }
    if (fault & MAX31865_FAULT_REFINLOW) {
      Serial.println("REFIN- > 0.85 x Bias"); 
    }
    if (fault & MAX31865_FAULT_REFINHIGH) {
      Serial.println("REFIN- < 0.85 x Bias - FORCE- open"); 
    }
    if (fault & MAX31865_FAULT_RTDINLOW) {
      Serial.println("RTDIN- < 0.85 x Bias - FORCE- open"); 
    }
    if (fault & MAX31865_FAULT_OVUV) {
      Serial.println("Under/Over voltage"); 
    }
    pt100.clearFault();
  }

  Serial.print(mlx.readAmbientTempC());
  Serial.print("\t");

  Temp_from_NTC_R(ntc0, B_540G);
  Serial.print("\t");
  Temp_from_NTC_R(ntc1, B_103JT);
  Serial.print("\t");
  Temp_from_NTC_R(ntc2, B_103JT);
  Serial.print("\t");
  Temp_from_NTC_R(ntc3, B_103F);
  Serial.println("");

  delay(1000*timeStep);

}
