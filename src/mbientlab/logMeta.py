#!/usr/bin/python3
from mbientlab.metawear import MetaWear, libmetawear, parse_value, create_voidp, create_voidp_int, cbindings
from time import sleep
from threading import Event

import sys
import os
import math
import optparse
import requests
import datetime



class LogMeta:
  parser = optparse.OptionParser(usage = "%prog [options] <sensor> \n\n"
  "start streaming data from bluetooth sensor and write data into InfluxDB\n\n"
  "")
  parser.add_option("-d" , "--duration", action="store", dest="duration", help="stream duration in s. Default 5 s", default=5)
  parser.add_option("-p" , "--period", action="store", dest="period", help="period between measures in s. Default 1 s", default=1)
  parser.add_option("-r" , "--rate", action="store", dest="rate", help="output data rate for accelerometer. Default 100 Hz, Range 25-800 Hz) ", default=100)
  parser.add_option("-s" , "--sleep", action="store_true", dest="sleep", help="put sensor to sleep after streaming", default=False)
  parser.add_option("-t" , "--token", action="store", dest="token", help="token to connect to InfluxDB", default="")
  parser.add_option("-f" , "--file", action="store", dest="file", help="file to download data", default="")
  parser.add_option("-F" , "--fullreset", action="store_true", dest="fullreset", help="Performs a full reset after acquisition", default=False)
  parser.add_option("-T" , "--temperature", action="store_true", dest="temperature", help="Measure temperature", default=False)
  parser.add_option("-H" , "--humidity", action="store_true", dest="humidity", help="Measure humidity", default=False)
  parser.add_option("-P" , "--pressure", action="store_true", dest="pressure", help="Measure pressure", default=False)
  parser.add_option("-L" , "--light", action="store_true", dest="light", help="Measure light", default=False)
  parser.add_option("-A" , "--accelerometer", action="store_true", dest="accelerometer", help="Measure acceleration", default=False)
  parser.add_option("-G" , "--gyroscope", action="store_true", dest="gyroscope", help="Measure gyroscope angles", default=False)

  def __init__(self):
    self.token=None
    self.strType=""
    self.strSensor=""
    self.noError=True
    self.filDownload=False
    self.nbData=0
    self.timBegin=0
    self.timEnd=0
    self.arrLoad=[]

  def processLog (self, ctx, p): 
        #global strType, strSensor, options, filDownload, noError, nbData, timBegin, timEnd
        self.nbData+=1
        if not self.timBegin: self.timBegin=p.contents.epoch/1000
        if self.strType!='A' and self.strType!='G' : print("{epoch: %d, value: %s, sensor: %s, type: %s}" % (p.contents.epoch, parse_value(p), self.strSensor, self.strType))
        pload="measure,sensor="+self.strSensor+" "
        val=parse_value(p)
        if self.strType=='T':
            pload+="temperature="+str(val)
        elif self.strType=='H':
            pload+="humidity="+str(val)
        elif self.strType=='L':
            pload+="light="+str(val)
        elif self.strType=='P':
            pload+="pressure="+str(val)
        elif self.strType=='A':
            pload+="x-axis=%f,y-axis=%f,z-axis=%f"%(val.x, val.y, val.z)
        elif self.strType=='G':
            pload+="x-gyro=%f,y-gyro=%f,z-gyro=%f"%(val.x, val.y, val.z)

        if self.timEnd==p.contents.epoch/1000: #to avoid identical timestamps
            pload+= ' '+str(p.contents.epoch+1)+ "000000"
            self.timEnd=(p.contents.epoch+1)/1000
        else :
            pload+= ' '+str(p.contents.epoch)+ "000000"
            self.timEnd=(p.contents.epoch)/1000

        if self.filDownload:
            self.filDownload.write(pload+'\n')
        elif self.token:
            self.arrLoad.append(pload)

  def importMemToDb(self):
        url="http://localhost:8086/api/v2/write?org=IPHC&bucket=ladder"
        headers={"Authorization": "Token "+self.token}
        for pload in self.arrLoad:
            response=requests.post(url=url, data=pload, headers=headers)
            if self.noError and response.status_code!=204:
                print(response.text);    
                self.noError=False

  def main(self, options, sensor):
        self.nbData=0
        self.timBegin=0
        mapSensor={"mt1":"C0:32:13:F3:E2:C9", "mt2":"D7:97:11:3A:8B:9D","mt3":"C7:93:84:4F:A0:1F", "mt4":"D5:F3:2F:05:11:68"}
        if options.file: 
            self.filDownload=open(options.file, "w")

        self.token=options.token
        if not self.token:
            if "INFLUXDB_TOKEN" in os.environ:
                self.token=os.environ['INFLUXDB_TOKEN']
            else:
                print("No token provided to authenticate to InfluxDB. Use --token option or INFLUXDB_TOKEN environment variable")

        print("Searching for device...")
        bAcq = options.temperature or options.humidity or options.light or options.pressure or options.accelerometer or options.gyroscope
        self.strSensor=sensor
        d = MetaWear(mapSensor[sensor] if sensor in mapSensor else sensor)
        try:
            d.connect()
            print(str(datetime.datetime.now())+" Connected to " + d.address)

            signalA = libmetawear.mbl_mw_acc_get_acceleration_data_signal(d.board)
            loggerA = create_voidp(lambda fn: libmetawear.mbl_mw_datasignal_log(signalA, None, fn), resource = "acc_logger")
            signalG = libmetawear.mbl_mw_gyro_bmi160_get_rotation_data_signal(d.board)
            loggerG = create_voidp(lambda fn: libmetawear.mbl_mw_datasignal_log(signalG, None, fn), resource = "gyro_logger")

            print(str(datetime.datetime.now())+" Configuring "+sensor)
            if options.accelerometer:
                libmetawear.mbl_mw_acc_set_odr(d.board, int(options.rate))
                libmetawear.mbl_mw_acc_set_range(d.board, 8.0)
                libmetawear.mbl_mw_acc_write_acceleration_config(d.board)
                sleep(1.0)
                #libmetawear.mbl_mw_acc_read_acceleration_config(d.board, None, )
            if options.gyroscope:
                libmetawear.mbl_mw_gyro_bmi160_set_range(d.board, cbindings.GyroBoschRange._500dps)
                libmetawear.mbl_mw_gyro_bmi160_set_odr(d.board, int(math.log(int(options.rate)/25)/math.log(2))+6) #cbindings.GyroBoschOdr constants
                libmetawear.mbl_mw_gyro_bmi160_write_config(d.board)

            if bAcq:
                print(str(datetime.datetime.now())+" Logging data for %ds %s"%(int(options.duration), sensor))
                libmetawear.mbl_mw_logging_start(d.board, 0)
            if options.accelerometer:
                libmetawear.mbl_mw_acc_enable_acceleration_sampling(d.board)
                libmetawear.mbl_mw_acc_start(d.board)
            if options.gyroscope:
                libmetawear.mbl_mw_gyro_bmi160_enable_rotation_sampling(d.board)
                libmetawear.mbl_mw_gyro_bmi160_start(d.board)

            if bAcq:
                sleep(int(options.duration))

            if options.accelerometer:
                libmetawear.mbl_mw_acc_stop(d.board)
                libmetawear.mbl_mw_acc_disable_acceleration_sampling(d.board)
            if options.gyroscope:
                libmetawear.mbl_mw_gyro_bmi160_stop(d.board)
                libmetawear.mbl_mw_gyro_bmi160_disable_rotation_sampling(d.board)

            if bAcq:
                libmetawear.mbl_mw_logging_stop(d.board) 
                libmetawear.mbl_mw_settings_set_connection_parameters(d.board, 7.5, 7.5, 0, 6000)
                sleep(1.0)
                print(str(datetime.datetime.now())+ " Downloading data from "+sensor)
                e = Event()

            def progress_update_handler(context, entries_left, total_entries):
                if (entries_left == 0):
                    e.set()

            if bAcq:
                fn_wrapper = cbindings.FnVoid_VoidP_UInt_UInt(progress_update_handler)
                download_handler = cbindings.LogDownloadHandler(context = None, \
                    received_progress_update = fn_wrapper, \
                    received_unknown_entry = cbindings.cast(None, cbindings.FnVoid_VoidP_UByte_Long_UByteP_UByte), \
                    received_unhandled_entry = cbindings.cast(None, cbindings.FnVoid_VoidP_DataP))
                callback = cbindings.FnVoid_VoidP_DataP(self.processLog)

            if options.accelerometer:
                self.strType='A'
                libmetawear.mbl_mw_logger_subscribe(loggerA, None, callback)
                libmetawear.mbl_mw_logging_download(d.board, 0, cbindings.byref(download_handler))
                print(str(datetime.datetime.now())+" Accelerometer download "+sensor)
            if options.gyroscope:
                self.strType='G'
                print(str(datetime.datetime.now())+" Gyroscope data "+sensor)
                libmetawear.mbl_mw_logger_subscribe(loggerG, None, callback)
                libmetawear.mbl_mw_logging_download(d.board, 0, cbindings.byref(download_handler))
            if bAcq:
                e.wait()
                print(str(datetime.datetime.now())+" waited "+sensor)
                if self.filDownload: self.filDownload.close()
                print (str(datetime.datetime.now())+" Download complete "+sensor)
                print("%d data lines from %s to %s (%d Hz) %s"%(self.nbData, datetime.datetime.fromtimestamp(self.timBegin).strftime("%X"), 
                        datetime.datetime.fromtimestamp(self.timEnd).strftime("%X"), self.nbData/(self.timEnd-self.timBegin) if self.timEnd!=self.timBegin else -1, sensor))
        finally:
            e = Event()
            d.on_disconnect = lambda status: e.set()
            if options.fullreset:
                libmetawear.mbl_mw_logging_clear_entries(d.board)
                libmetawear.mbl_mw_macro_erase_all(d.board)
                print(str(datetime.datetime.now())+" reset after garbage collection "+sensor)
                libmetawear.mbl_mw_debug_reset_after_gc(d.board)
            else:
                print(str(datetime.datetime.now())+" Resetting device "+sensor)

            libmetawear.mbl_mw_debug_reset(d.board)
            e.wait()
            if self.token and len(self.arrLoad): 
                print(str(datetime.datetime.now())+" Import into DB "+sensor)
                self.importMemToDb()

            print(str(datetime.datetime.now())+" Done "+sensor)

if __name__ == '__main__':
    (options, args) = LogMeta.parser.parse_args()
    if len(args)<1:
        LogMeta.parser.print_help()
        sys.exit()
    else:
        log = LogMeta()
        log.main(options, args[0])
