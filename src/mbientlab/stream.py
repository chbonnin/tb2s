#!/usr/bin/python3
from mbientlab.metawear import MetaWear, libmetawear, parse_value, create_voidp, create_voidp_int, cbindings
from time import sleep
from threading import Event

import sys
import os
import math
import optparse
import requests
import datetime
import json

class Stream:
    parser = optparse.OptionParser(usage = "%prog [options] <sensor> \n\n"
            "start streaming data from bluetooth sensor and write data into InfluxDB\n\n"
            ""
            )
    parser.add_option("-d" , "--duration", action="store", dest="duration", help="stream duration in s. Default 5 s", default=5)
    parser.add_option("-p" , "--period", action="store", dest="period", help="period between measures in s. Default 1 s", default=1)
    parser.add_option("-r" , "--rate", action="store", dest="rate", help="output data rate for accelerometer. Default 100 Hz, Range 25-800 Hz) ", default=100)
    parser.add_option("-s" , "--sleep", action="store_true", dest="sleep", help="put sensor to sleep after streaming", default=False)
    parser.add_option("-t" , "--token", action="store", dest="token", help="token to connect to InfluxDB", default="")
    parser.add_option("-T" , "--temperature", action="store_true", dest="temperature", help="Measure temperature", default=False)
    parser.add_option("-H" , "--humidity", action="store_true", dest="humidity", help="Measure humidity", default=False)
    parser.add_option("-P" , "--pressure", action="store_true", dest="pressure", help="Measure pressure", default=False)
    parser.add_option("-L" , "--light", action="store_true", dest="light", help="Measure light", default=False)
    parser.add_option("-A" , "--accelerometer", action="store_true", dest="accelerometer", help="Measure acceleration", default=False)
    parser.add_option("-G" , "--gyroscope", action="store_true", dest="gyroscope", help="Measure gyroscope angles", default=False)
    parser.add_option("-m" , "--memory", action="store_true", dest="memory", help="Store data in memory before sending it to DB", default=False)

    def __init__(self):
        self.token=None
        self.nbData=0
        self.timBegin=0
        self.timEnd=0
        self.mem=None

    def processData (self, ctx, p):
        #global mem, options, nbData, timBegin, timEnd
        self.nbData+=1
        if not self.timBegin: self.timBegin=p.contents.epoch/1000
        self.timEnd=p.contents.epoch/1000
        self.strType=chr((ctx>>8)&255)
        self.strSensor=chr(ctx&255)
        if self.strType!='A' and self.strType!='G' : print("{epoch: %d, value: %s, sensor: %s, type: %s}" % (p.contents.epoch, parse_value(p), self.strSensor, self.strType))
        if self.token:
            url="http://localhost:8086/api/v2/write?org=IPHC&bucket=ladder"
            pload="measure,sensor=mt"+self.strSensor+" "
            headers={"Authorization": "Token "+self.token}
            val=parse_value(p)
            if self.strType=='T':
                pload+="temperature="+str(val)
            elif self.strType=='H':
                pload+="humidity="+str(val)
            elif self.strType=='L':
                pload+="light="+str(val)
            elif self.strType=='P':
                pload+="pressure="+str(val)
            elif self.strType=='A':
                pload+="x-axis=%f,y-axis=%f,z-axis=%f"%(val.x, val.y, val.z)
            elif self.strType=='G':
                pload+="x-gyro=%f,y-gyro=%f,z-gyro=%f"%(val.x, val.y, val.z)
            
            if self.mem!=None:
                self.mem.append(pload+' '+str(p.contents.epoch))
                #mem.append( "{epoch : %d, extra : %d, value : %d, type_id : %d, length : %d}" % (p.contents.epoch, p.contents.extra, p.contents.value, p.contents.type_id, p.contents.length))
            else:
                response=requests.post(url=url, data=pload, headers=headers)
                #print("Response to:",pload,":", response.status_code, response.text)

    def main(self, options, name):
        mapSensor={"mt1":"C0:32:13:F3:E2:C9", "mt2":"D7:97:11:3A:8B:9D","mt3":"C7:93:84:4F:A0:1F", "mt4":"D5:F3:2F:05:11:68"}
        self.token=options.token
        sensor=name
        if not self.token:
            if "INFLUXDB_TOKEN" in os.environ:
                self.token=os.environ['INFLUXDB_TOKEN']
            else:
                print("No token provided to authenticate to InfluxDB. Use --token option or INFLUXDB_TOKEN environment variable")

        bAcq = options.temperature or options.humidity or options.light or options.pressure or options.accelerometer or options.gyroscope
        if hasattr(options, "memory") and options.memory: 
            self.mem=[]

        print("Searching for device...")
        d = MetaWear(mapSensor[sensor] if sensor in mapSensor else sensor)
        try:
            d.connect()
            print("Connected to " + d.address)
            e = Event()

            callback = cbindings.FnVoid_VoidP_DataP(self.processData)

            print("Configuring device")
            if options.temperature: 
                signalT = libmetawear.mbl_mw_multi_chnl_temp_get_temperature_data_signal(d.board, cbindings.MetaWearRProChannel.ON_BOARD_THERMISTOR)
                libmetawear.mbl_mw_datasignal_subscribe(signalT, ord('T')<<8 | ord(sensor[-1]), callback)

            if options.humidity:
                signalH = libmetawear.mbl_mw_humidity_bme280_get_percentage_data_signal(d.board)
                libmetawear.mbl_mw_datasignal_subscribe(signalH, ord('H')<<8 | ord(sensor[-1]), callback)

            if options.light:
                #libmetawear.mbl_mw_als_ltr329_set_gain(d.board, cbindings.AlsLtr329Gain._1X)
                #libmetawear.mbl_mw_als_ltr329_set_integration_time(d.board, cbindings.AlsLtr329IntegrationTime._400ms)
                #libmetawear.mbl_mw_als_ltr329_set_measurement_rate(d.board, cbindings.AlsLtr329MeasurementRate._100ms)
                #libmetawear.mbl_mw_als_ltr329_write_config(d.board)
                signalL = libmetawear.mbl_mw_als_ltr329_get_illuminance_data_signal(d.board)
                libmetawear.mbl_mw_datasignal_subscribe(signalL, ord('L')<<8 | ord(sensor[-1]), callback)

            if options.pressure:
                signalP = libmetawear.mbl_mw_baro_bosch_get_pressure_read_data_signal(d.board)
                libmetawear.mbl_mw_datasignal_subscribe(signalP, ord('P')<<8 | ord(sensor[-1]), callback)

            if options.accelerometer:
                libmetawear.mbl_mw_settings_set_connection_parameters(d.board, 7.5, 7.5, 0, 6000)
                sleep(1.5)
                libmetawear.mbl_mw_acc_set_odr(d.board, int(options.rate))
                libmetawear.mbl_mw_acc_set_range(d.board, 16.0)
                libmetawear.mbl_mw_acc_write_acceleration_config(d.board)
                signalA = libmetawear.mbl_mw_acc_get_acceleration_data_signal(d.board)
                libmetawear.mbl_mw_datasignal_subscribe(signalA, ord('A')<<8 | ord(sensor[-1]), callback) 

            if options.gyroscope:
                libmetawear.mbl_mw_gyro_bmi160_set_range(d.board, cbindings.GyroBoschRange._500dps)
                libmetawear.mbl_mw_gyro_bmi160_set_odr(d.board, int(math.log(int(options.rate)/25)/math.log(2))+6) #cbindings.GyroBoschOdr constants
                libmetawear.mbl_mw_gyro_bmi160_write_config(d.board)
                signalG = libmetawear.mbl_mw_gyro_bmi160_get_rotation_data_signal(d.board)
                libmetawear.mbl_mw_datasignal_subscribe(signalG, ord('G')<<8 | ord(sensor[-1]), callback)
                sleep(2)

            if bAcq:
                timer = create_voidp(lambda fn: libmetawear.mbl_mw_timer_create_indefinite(d.board, int(options.period)*1000, 0, None, fn), resource = "timer", event = e) 
                libmetawear.mbl_mw_event_record_commands(timer)

            if options.temperature: libmetawear.mbl_mw_datasignal_read(signalT)
            if options.humidity: libmetawear.mbl_mw_datasignal_read(signalH)
            if options.light: libmetawear.mbl_mw_datasignal_read(signalL)
            if options.pressure: libmetawear.mbl_mw_datasignal_read(signalP)
            if options.accelerometer: 
                libmetawear.mbl_mw_acc_enable_acceleration_sampling(d.board)
                libmetawear.mbl_mw_acc_start(d.board)
            if options.gyroscope:
                libmetawear.mbl_mw_gyro_bmi160_enable_rotation_sampling(d.board)
                libmetawear.mbl_mw_gyro_bmi160_start(d.board)


            if bAcq:
                create_voidp_int(lambda fn: libmetawear.mbl_mw_event_end_record(timer, None, fn), event = e) 
                libmetawear.mbl_mw_timer_start(timer) 
                print(datetime.datetime.now(), ": Stream data for %s s"%options.duration)
                sleep(int(options.duration)) 
                libmetawear.mbl_mw_timer_remove(timer)
            if options.temperature: libmetawear.mbl_mw_datasignal_unsubscribe(signalT)
            if options.humidity: libmetawear.mbl_mw_datasignal_unsubscribe(signalH)
            if options.light: libmetawear.mbl_mw_datasignal_unsubscribe(signalL)
            if options.pressure: libmetawear.mbl_mw_datasignal_unsubscribe(signalP)
            if options.accelerometer: 
                libmetawear.mbl_mw_acc_stop(d.board)
                libmetawear.mbl_mw_acc_disable_acceleration_sampling(d.board)
                libmetawear.mbl_mw_datasignal_unsubscribe(signalA)
            if options.gyroscope:
                libmetawear.mbl_mw_gyro_bmi160_stop(d.board)
                libmetawear.mbl_mw_gyro_bmi160_disable_rotation_sampling(d.board)
                libmetawear.mbl_mw_datasignal_unsubscribe(signalG)

            sleep(1.0)

            print("Resetting device")
            e = Event()
            d.on_disconnect = lambda status: e.set()
            if options.sleep: libmetawear.mbl_mw_debug_enable_power_save(d.board)
            libmetawear.mbl_mw_debug_reset(d.board)
            e.wait()
            if hasattr(options, "memory") and options.memory:
                f=open("/tmp/mem.influx","w")
                for pload in self.mem:
                    url="http://localhost:8086/api/v2/write?org=IPHC&bucket=ladder"
                    headers={"Authorization": "Token "+self.token}
                    response=requests.post(url=url, data=pload, headers=headers)
                    f.write(pload+'000000\n')
                    
                f.close()

            if self.timEnd:
                print("%d data lines from %s to %s (%d Hz)"%(self.nbData, datetime.datetime.fromtimestamp(self.timBegin).strftime("%X"), 
                    datetime.datetime.fromtimestamp(self.timEnd).strftime("%X"), self.nbData/(self.timEnd-self.timBegin)))
        except Exception as ex:
            print(ex)

if __name__ == '__main__':
    (options, args) = Stream.parser.parse_args()
    if len(args)<1:
        Stream.parser.print_help()
        sys.exit()
    else:
        stm=Stream()
        stm.main(options, args[0])
