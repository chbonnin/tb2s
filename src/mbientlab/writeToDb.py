#!/usr/bin/python3
import requests
import os
import sys

url="http://localhost:8086/api/v2/write?org=IPHC&bucket=ladder"
if "INFLUXDB_TOKEN" in os.environ:
    token=os.environ['INFLUXDB_TOKEN']
else:
    print("No token provided to authenticate to InfluxDB. Use --token option or INFLUXDB_TOKEN environment variable")

headers={"Authorization": "Token "+token}

f=open(sys.argv[1],'r')
for pload in f:
    response=requests.post(url=url, data=pload, headers=headers)
    if response.status_code!=204:
        print(response.text);    
        break
