#!/usr/bin/python3
import datetime
import sys

f=open(sys.argv[1],'r')
header=""
for pload in f:
    arrFields=pload.split(" ")
    arrTag=arrFields[0].split(",")[1:]
    arrVal=arrFields[1].split(",")
    if not header:
        header="measure,"
        for tag in arrTag:
            header+=tag.split("=")[0]+","
        for val in arrVal:
            header+=val.split("=")[0]+","
    
        header+="time"
        print(header)

    data=arrFields[0].split(",")[0]+","
    for tag in arrTag:
        data+=tag.split("=")[1]+","
    for val in arrVal:
        data+=val.split("=")[1]+","

    data+=datetime.datetime.fromtimestamp(int(arrFields[2])/1e9).strftime("%Y-%m-%dT%H:%M:%S")
    print(data)

