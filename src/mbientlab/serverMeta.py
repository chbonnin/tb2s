#!/usr/bin/python3
# -*- coding: utf8 -*-  

import threading
import datetime
import os
import sys
import re
import requests
import flask
import stream
import logMeta

app = flask.Flask(__name__)
lstTask=[]
strToken = os.environ.get("INFLUXDB_TOKEN") if "INFLUXDB_TOKEN" in os.environ else "no token provided!"

class Options:
  def __init__(self, token, temperature, humidity, pressure, light, accelerometer, gyroscope, rate, period, duration, sleep, reset):
    self.token=token
    self.temperature=temperature
    self.humidity=humidity
    self.light=light
    self.pressure=pressure
    self.accelerometer=accelerometer
    self.gyroscope=gyroscope
    self.rate=rate
    self.period=period
    self.duration=duration
    self.fullreset=reset
    self.sleep=sleep
    self.file=None
    self.memory=None

#Ecran principal 
@app.route('/metatracker/<action>', methods=['GET','POST'])
def metatracker(action):
    global lstTask
    datBegin=datetime.datetime.now()
    lstTask=list(filter(lambda x :x['end']+datetime.timedelta(0,5) >= datBegin, lstTask)) #remove expired tasks

    if action=="quit":
        func = flask.request.environ.get('werkzeug.server.shutdown')
        func()
    if action=="stream": 
        strUnit='s'
        strDur=flask.request.form.get('duration').lower()
        if len(strDur)>0: 
          if strDur[-1] in ['s', 'm', 'h', 'd']:
            strUnit=strDur[-1]
            strDur=strDur[:-1].strip()
        else:   
            strDur=5

        if strUnit=='d':
            duration=int(strDur)*86400
        elif strUnit=='h':
            duration=int(strDur)*3600
        elif strUnit=='m':
            duration=int(strDur)*60
        else:
            duration=int(strDur)

        opt=Options(flask.request.form.get('token'), 
                    flask.request.form.get('temperature')=="on",
                    flask.request.form.get('humidity')=="on",
                    flask.request.form.get('pressure')=="on",
                    flask.request.form.get('light')=="on",
                    flask.request.form.get('accelerometer')=="on",
                    flask.request.form.get('gyroscope')=="on",
                    flask.request.form.get('rate'),
                    flask.request.form.get('period'),    
                    duration,
                    flask.request.form.get('sleep')=="on",
                    flask.request.form.get('fullreset')=="on")
        for iSens in range(1,5):
            strName="mt%d"%iSens
            if flask.request.form.get(strName)=="on":
                if not any(task['name']==strName for task in lstTask): #check if this sensor name is already in list
                    if flask.request.form.get('acq')=="stream":
                        stm=stream.Stream()
                        threading.Thread(target=stm.main, args=(opt, strName), daemon=False).start()
                        lstTask.append({"name":strName, "begin":datBegin, "end":datBegin+datetime.timedelta(0,duration+3), "type":"streaming"})
                    else:
                        log=logMeta.LogMeta()
                        threading.Thread(target=log.main, args=(opt, strName), daemon=False).start()
                        lstTask.append({"name":strName, "begin":datBegin, "end":datBegin+datetime.timedelta(0,duration+3), "type":"logging"})
    elif action=="import" :
        f = flask.request.files['importFile']
        f.save("download/"+f.filename)
        if f.filename.endswith(".influx"):
          lstTask.append({"name":"importation", "begin":datBegin, "end":datBegin+datetime.timedelta(0,2), "type":"InfluxDB"})
          with open("download/"+f.filename) as fCsv:
            url="http://localhost:8086/api/v2/write?org=IPHC&bucket=ladder"
            nbLine=0
            for line in fCsv:
                response=requests.post(url=url, data=line, headers={"Authorization": "Token "+strToken})
                if response.status_code!=204:
                    print(response.text);    
                    break
                else:
                    nbLine+=1

            print ("%d lines imported"%nbLine)
        elif f.filename.endswith(".csv"):
            match=re.search(r"(mt[^_]+)_", f.filename)
            if match:
                print("import data for sensor %s"%match.group(1))
                os.system("docker cp download/%s influx:/home/influxdb/data/%s"%(f.filename, f.filename))
                strCmd="docker exec -it influx bash -c '/home/influxdb/data/importData.sh %s %s /home/influxdb/data/%s'"%(match.group(1), strToken, f.filename)
                print(strCmd)
                os.system(strCmd)
            else:
                print("No sensor name (mt<N>) found in filename %s"%f.filename)

    return flask.render_template('main.html', data=lstTask, token=strToken)

if __name__ == '__main__':
        app.run(debug = True, host='0.0.0.0', port=8000)
