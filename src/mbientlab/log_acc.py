# usage: python log_acc.py [mac]
from __future__ import print_function
from mbientlab.metawear import MetaWear, libmetawear, parse_value, create_voidp, create_voidp_int
from mbientlab.metawear.cbindings import *
from time import sleep
from threading import Event

import sys
import optparse

def processLog (ctx, p): 
    print("{epoch: %d, value: %s}" % (p.contents.epoch, parse_value(p)), file=filDownload if filDownload else sys.stdout)
 
filDownload=False
parser = optparse.OptionParser(usage = "%prog [options] <sensor> \n\n"
"start streaming data from bluetooth sensor and write data into InfluxDB\n\n"
"")
parser.add_option("-d" , "--duration", action="store", dest="duration", help="stream duration in s. Default 5 s", default=5)
parser.add_option("-p" , "--period", action="store", dest="period", help="period between measures in s. Default 1 s", default=1)
parser.add_option("-r" , "--rate", action="store", dest="rate", help="output data rate for accelerometer. Default 100 Hz, Range 25-800 Hz) ", default=100)
parser.add_option("-s" , "--sleep", action="store_true", dest="sleep", help="put sensor to sleep after streaming", default=False)
parser.add_option("-t" , "--token", action="store", dest="token", help="token to connect to InfluxDB", default="")
parser.add_option("-f" , "--file", action="store", dest="file", help="file to download data", default="")
parser.add_option("-F" , "--fullreset", action="store_true", dest="fullreset", help="Performs a full reset after acquisition", default=False)
parser.add_option("-T" , "--temperature", action="store_true", dest="temperature", help="Measure temperature", default=False)
parser.add_option("-H" , "--humidity", action="store_true", dest="humidity", help="Measure humidity", default=False)
parser.add_option("-P" , "--pressure", action="store_true", dest="pressure", help="Measure pressure", default=False)
parser.add_option("-L" , "--light", action="store_true", dest="light", help="Measure light", default=False)
parser.add_option("-A" , "--accelerometer", action="store_true", dest="accelerometer", help="Measure acceleration", default=False)
parser.add_option("-G" , "--gyroscope", action="store_true", dest="gyroscope", help="Measure gyroscope angles", default=False)
(options, args) = parser.parse_args()
if options.file: 
    filDownload=open(options.file, "w")

print("Searching for device...")
mapSensor={"mt1":"C0:32:13:F3:E2:C9", "mt2":"D7:97:11:3A:8B:9D","mt3":"C7:93:84:4F:A0:1F", "mt4":"D5:F3:2F:05:11:68"}
strSensor=mapSensor[sys.argv[1]] if sys.argv[1].startswith("mt") else sys.argv[1]
d = MetaWear(strSensor)
d.connect()
print("Connected to " + d.address)

print("Configuring device")

try:
    signal = libmetawear.mbl_mw_acc_get_acceleration_data_signal(d.board)
    logger = create_voidp(lambda fn: libmetawear.mbl_mw_datasignal_log(signal, None, fn), resource = "acc_logger")
    
    if options.rate:
      print("write acc config: "+str(options.rate))
      libmetawear.mbl_mw_acc_set_odr(d.board, int(options.rate))
      #libmetawear.mbl_mw_acc_set_range(d.board, 8.0)
      libmetawear.mbl_mw_acc_write_acceleration_config(d.board)
      sleep(1.0)

    libmetawear.mbl_mw_logging_start(d.board, 0)
    libmetawear.mbl_mw_acc_enable_acceleration_sampling(d.board)
    libmetawear.mbl_mw_acc_start(d.board)

    print("Logging data for %s s"%options.duration)
    sleep(int(options.duration))

    libmetawear.mbl_mw_acc_stop(d.board)
    libmetawear.mbl_mw_acc_disable_acceleration_sampling(d.board)
    libmetawear.mbl_mw_logging_stop(d.board)

    print("Downloading data")
    libmetawear.mbl_mw_settings_set_connection_parameters(d.board, 7.5, 7.5, 0, 6000)
    sleep(1.0)

    e = Event()
    def progress_update_handler(context, entries_left, total_entries):
        if (entries_left == 0):
            e.set()
    
    fn_wrapper = FnVoid_VoidP_UInt_UInt(progress_update_handler)
    download_handler = LogDownloadHandler(context = None, \
        received_progress_update = fn_wrapper, \
        received_unknown_entry = cast(None, FnVoid_VoidP_UByte_Long_UByteP_UByte), \
        received_unhandled_entry = cast(None, FnVoid_VoidP_DataP))

    callback = FnVoid_VoidP_DataP(processLog)
    libmetawear.mbl_mw_logger_subscribe(logger, None, callback)
    libmetawear.mbl_mw_logging_download(d.board, 0, byref(download_handler))
    e.wait()
    if filDownload: filDownload.close()
	
except RuntimeError as err:
    print(err)
finally:
    print("Resetting device")
    
    e = Event()
    d.on_disconnect = lambda status: e.set()
    libmetawear.mbl_mw_debug_reset(d.board)
    e.wait()
