import socket
import time

# Define the host and port
HOST = '127.0.0.1'  # Localhost
PORT = 10001        # Port to listen on

# Create a socket object
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    # Bind the socket to the address and port
    s.bind((HOST, PORT))
    # Enable the server to accept connections
    s.listen()

    print(f"Listening on {HOST}:{PORT}...")
    while True:
        # Accept a connection from a client
        conn, addr = s.accept()
        with conn:
            print(f"Connected by {addr}")
            # Receive data from the client (buffer size is 1024)
            data = conn.recv(1024)
            if not data:
                time.sleep(1)
                continue
            # Print the received data (optional)
            print(f"Received: {data.decode()}")
            # Send "NOP" back to the client
            conn.sendall(b'42')
