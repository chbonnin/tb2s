#!/usr/bin/python3
# -*- coding: utf8 -*-

import threading
import time
import datetime
import requests
import socket
import json
import statistics
import flask
from dataclasses import dataclass, asdict
from influxdb_client import InfluxDBClient, Point, WritePrecision
from influxdb_client.client.write_api import SYNCHRONOUS
from statemachine import State, StateMachine

VERSION_NUMBER = "1.0"
BUILD_NUMBER = 0
NB_CHANNELS = 4
EPSILON = 1

app = flask.Flask(__name__)
errorMsg = ""

@dataclass
class Channel:
    num: int
    voltage: float
    current: float
    getVoltage: bool
    getCurrent: bool
    sendToDb: bool


@dataclass
class Config:
    host: str
    port: int
    delay: int
    channel: int
    min: float
    max: float
    step: float
    stabilisation: int
    average: int
    db_url: str
    db_org: str
    db_bucket: str
    db_token: str
    autorefresh: bool
    delta: int
    alert: float


class FSM(StateMachine):
    Ready = State(initial=True)
    Rising = State()
    Stabilising = State()
    Averaging = State()
    Returning = State()

    rise = Ready.to(Rising)
    next = (
        Rising.to(Stabilising, cond="is_objective_reached")
        | Stabilising.to(Averaging, cond="is_stable_duration_reached")
        | Averaging.to(Returning, cond="is_average_duration_reached and is_maximum_reached")
        | Averaging.to(Rising, cond="is_average_duration_reached and !is_maximum_reached")
        | Returning.to(Ready, cond="is_objective_reached")
    )
    stop = Rising.to(Returning) | Stabilising.to(Returning) | Averaging.to(Returning)

    def __init__(self, *args, **kwargs):
        self.objective: float = 0  # objective voltage value of this step
        self.voltage: float = 0  # current voltage value
        self.timeBegin: datetime.datetime = None  # start time at this state
        self.avgVoltage: float = 0  # voltage values mean for current step
        self.nbAvg: int = 0  # number of values for current step
        self.arrResult: [(float, float)] = []  # voltage and current mean values of each step
        self.arrCurrent: [float] = []  # current values of one step
        self.bAlert = False  # Alert flag: the current threshold value is over
        super().__init__(*args, **kwargs)

    def on_rise(self):
        self.objective = cfg.min
        self.arrResult = []
        setVoltage(cfg.min, cfg.channel, "ON")
        arrChannels[cfg.channel].getVoltage = True
        arrChannels[cfg.channel].getCurrent = True

    def on_next(self):
        self.timeBegin = datetime.datetime.now()

    def on_stop(self):
        self.objective = 0
        setVoltage(0, cfg.channel, "OFF")
        self.bAlert = False

    def on_enter_Returning(self):
        self.on_stop()

    def on_enter_Stabilising(self):
        if cfg.step > 0:
            self.objective = min(self.objective + cfg.step, cfg.max)
        else:
            self.objective = max(self.objective + cfg.step, cfg.max)

    def on_enter_Averaging(self):
        self.avgVoltage = 0
        self.arrCurrent = []
        self.nbAvg = 0

    def on_exit_Averaging(self):
        nbDelta = min(len(self.arrCurrent) // 2, cfg.delta)
        if nbDelta > 0:
            deltaI = statistics.mean(self.arrCurrent[-nbDelta:]) - statistics.mean(self.arrCurrent[:nbDelta])
        else:
            deltaI = 0

        self.arrResult.append(
            (
                self.avgVoltage,
                statistics.mean(self.arrCurrent),
                statistics.stdev(self.arrCurrent),
                deltaI,
            )
        )
        if not self.is_maximum_reached():
            setVoltage(self.objective, cfg.channel, "")

    def is_objective_reached(self):
        return abs(self.voltage - self.objective) < EPSILON

    def is_maximum_reached(self):
        return abs(self.voltage - cfg.max) < EPSILON

    def is_stable_duration_reached(self):
        return (datetime.datetime.now() - self.timeBegin).seconds > cfg.stabilisation

    def is_average_duration_reached(self):
        return (datetime.datetime.now() - self.timeBegin).seconds > cfg.average

    def describeState(self):
        strRet = self.current_state.name + ", "
        if self.current_state in [self.Rising, self.Returning]:
            strRet += f"objective: {self.objective} V"
        elif self.current_state in [self.Stabilising, self.Averaging]:
            duration = (datetime.datetime.now() - self.timeBegin).seconds
            end = cfg.stabilisation if self.current_state == self.Stabilising else cfg.average
            strRet += f"remaining: {end - duration} S"

        return strRet


def mainLoop():
    global fsm, device, errorMsg
    while cfg.delay:
        if errorMsg or device.getpeername() != ((cfg.host, cfg.port)):  # Device has changed
            try:
                device.close()
                device = socket.socket()
                device.settimeout(3)
                device.connect((cfg.host, cfg.port))
                errorMsg = ""
            except Exception as exc:
                print(exc)
                errorMsg = str(exc)
                continue

        if "next" in fsm.allowed_events:
            fsm.next()

        for ch in arrChannels:
            if ch.getCurrent:
                ch.current = getValue(device, "CURR", ch.num, ch.sendToDb)
                if ch.num == cfg.channel:
                    fsm.bAlert = abs(ch.current) > cfg.alert
                    if fsm.current_state == FSM.Averaging:
                        fsm.nbAvg += 1
                        fsm.arrCurrent.append(ch.current)

            if ch.getVoltage:
                ch.voltage = getValue(device, "VOLT", ch.num, ch.sendToDb)
                if ch.num == cfg.channel:
                    fsm.voltage = ch.voltage
                    if fsm.current_state == FSM.Averaging:
                        fsm.avgVoltage = (fsm.avgVoltage * (fsm.nbAvg - 1) + ch.voltage) / fsm.nbAvg

        time.sleep(cfg.delay)

    # device.close()


# Measure value from HV device
def getValue(device: socket.socket, strType: str, numCh: int, bSendToDb: bool):
    global errorMsg
    strCmd = f":MEAS:{strType}? (@{numCh})\r\n"

    try:
        device.send(strCmd.encode())
        retVal = float(device.recv(256).decode().strip()[:-1])
    except Exception as exc:
        print(exc)
        retVal = 0
        errorMsg = str(exc)
    # print(strCmd, retVal)
    if bSendToDb:
        upload_timeseries(f"{cfg.host}_{cfg.channel}", strType, retVal)

    return retVal


# Write value into Influx DB
def upload_timeseries(origin, field, value):
    try:
        with InfluxDBClient(
            url=cfg.db_url, token=cfg.db_token, org=cfg.db_org
        ) as client:
            write_api = client.write_api(write_options=SYNCHRONOUS)
            point = (
                Point("PowerSupply")
                .tag("host", origin)
                .tag("type", field)
                .field("value", value)
                .time(datetime.datetime.utcnow(), WritePrecision.NS)
            )
            write_api.write(cfg.db_bucket, cfg.db_org, point)
        # print("Upload successfull")
    except requests.exceptions.Timeout as ex:
        print("Time series upload timed out: %s", ex)
    except requests.exceptions.RequestException as ex:
        print("Time series upload failed: %s", ex)
    except Exception as err:
        print("Exception:", err)
        pass


def setVoltage(fVolt, numCh, strOn):
    global fsm, device, errorMsg
    strCmd = f":VOLT {fVolt}V,(@{numCh})\r\n"
    # print(strCmd)
    try:
        device.send(strCmd.encode())
        print(device.recv(256).decode())
        if strOn:
            strCmd = f":VOLT {strOn},(@{numCh})\r\n"
            # print(strCmd)
            device.send(strCmd.encode())
            print(device.recv(256).decode())
    except Exception as exc:
        print(exc)
        errorMsg = str(exc)


@app.route("/")
def index():
    global fsm, device, errorMsg
    bRunning = fsm.current_state in [FSM.Rising, FSM.Averaging, FSM.Stabilising]
    return flask.render_template(
        "index.html",
        channels=arrChannels,
        config=cfg,
        version=f"{VERSION_NUMBER}.{BUILD_NUMBER}",
        fsm_state=fsm.describeState(),
        fsm_running=bRunning,
        available_result=len(fsm.arrResult) > 0,
        alert=fsm.bAlert,
        error=errorMsg,
    )


@app.route("/save_config", methods=["POST"])
def saveConfig():
    global cfg
    cfg = Config(
        host=flask.request.form["host"],
        port=int(flask.request.form["port"]),
        delay=int(flask.request.form["delay"]),
        channel=int(flask.request.form["channel"]),
        min=float(flask.request.form["min"]),
        max=float(flask.request.form["max"]),
        step=float(flask.request.form["step"]),
        stabilisation=int(flask.request.form["stabilisation"]),
        average=int(flask.request.form["average"]),
        db_url=cfg.db_url,
        db_org=cfg.db_org,
        db_bucket=cfg.db_bucket,
        db_token=cfg.db_token,
        autorefresh=("autorefresh" in flask.request.form and flask.request.form["autorefresh"] == "on"),
        delta=int(flask.request.form["delta"]),
        alert=float(flask.request.form["alert"]),
    )
    with open("config.json", "w") as filCfg:
        json.dump(asdict(cfg), filCfg, indent=4)

    return flask.redirect("/")


@app.route("/send_command", methods=["POST"])
def sendCommand():
    strCmd = flask.request.form["cmd"] + "\r\n"
    device.send(strCmd.encode())
    strAns = device.recv(256).decode().strip()[:-1]
    print(strAns)
    return flask.make_response(strAns)


# Request from browser when a box is checked
@app.route("/change_checkbox", methods=["POST"])
def changeCheckbox():
    strId = flask.request.form["id"]
    strVal = flask.request.form["checked"]
    num = int(strId[-1])
    if strId.startswith("Volt"):
        arrChannels[num].getVoltage = strVal == "true"
    elif strId.startswith("Curr"):
        arrChannels[num].getCurrent = strVal == "true"
    elif strId.startswith("Influx"):
        arrChannels[num].sendToDb = strVal == "true"

    return flask.make_response(strVal)


@app.route("/start_iv", methods=["POST"])
def startIV():
    global fsm
    if fsm.current_state == FSM.Ready:
        fsm.rise()
    else:
        fsm.stop()

    return flask.make_response(fsm.current_state.name)


@app.route("/download_result", methods=["POST", "GET"])
def downloadResult():
    global fsm
    return flask.Response(
        "Voltage,Current mean,Standard Deviation, Delta I\n"
        + "\n".join(map(lambda t: f"{t[0]:.5g},{t[1]:.5g},{t[2]:.5g},{t[3]:.5g}", fsm.arrResult)),
        mimetype="text/plain",
        headers={"Content-disposition": "attachment; filename=result.csv"},
    )


try:
    cfg = Config(**json.load(open("config.json", "r")))
except Exception as exc:
    print(exc)
    cfg = Config("sbgps26", 10001, 5, 3, 0, 100, 10, 30, 30, "", "", "", "", False, 5, 1e-6)

arrChannels = list(
    map(lambda i: Channel(i, 0, 0, False, False, False), range(NB_CHANNELS))
)
device = socket.socket()
device.settimeout(3)
print(f"Connection to: {cfg.host}")
device.connect((cfg.host, cfg.port))
fsm = FSM(allow_event_without_transition=True)
threading.Thread(target=mainLoop, daemon=False).start()
