# High Voltage Measures
## Documentation
Mini application Web permettant de réaliser des scans courant/tension, de lire les mesures sur une alimentation Haute Tension ISE modèle SHR 4220 et d'écrire les données dans une base InfluxDB.

Les paramètres se configurent dans la page et sont enregistrés dans le fichier config.json

Le seul paramètre qui manque est la pente de la tension car, bizarrement, elle ne peut pas être modifiée en ligne, on ne peut la changer que sur l'appareil lui-même.

Il y a des cases à cocher pour chaque voie pour décider les mesures que l'on souhaite lire et enregistrer en base de donnée

L'interrupteur __IV Scan__ permet de lancer une procédure de scan IV comprenant les états suivants :

 * Rising : le temps que la tension suivante soit atteinte
 * Stabilising : temps de stabilisation
 * Averaging : fenetre de calcul de la moyenne

Le script boucle sur ces trois états le temps d'arriver au maximum

Le résultat peut être téléchargé dans un fichier CSV

La procédure peut être interrompue à tout moment en recliquant sur le bouton __Scan__. Un message en rouge s'affiche en cas de dépassement du seuil de courant fixé
## Installation

Dans le répertoire  tb2s/src/ise : 

    python -m venv .venv
    
    source .venv/bin/activate
 
    pip install -r requirements.txt 
    
    sudo firewall-cmd --add-port=5000/tcp --permanent
    
    sudo firewall-cmd --reload

## Lancement
 
    ./runFlash.sh

## Utilisation

http://localhost:5000/



