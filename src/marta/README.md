# MARTA remote control
The Monoblock Approach for a Refrigeration Technical Application can be controlled using VNC for manual operations but also provides a MODBUS protocol interface for further operation like actions automatization or monitoring measures.
This NodeJS application allows:
* reading of all availables measures at once
* reading values regularly and writing into an InfluxDB database
* display alarm status 
* set some parameters 
* launch actions


# Installation on server

This is an NodeJS (ExpressJS) application that needs **npm** to be installed
`sudo apt install npm`

`npm install`

The IP address of the MARTA device has to be configured in this file `connection.json`

# Start the application

`npm start`

In a web browser, go to this URL (replace localhost by your computer's address): 


[http://localhost:3000](http://localhost:3000)

:warning: Your firewall must be configured to make port 3000 opened

# Application pages
## Main menu

![Main menu](doc/martaHome.png)

## Read values
![Read values](doc/martaValues.png)
The values description, tooltip and legend tables are taken from MARTA User's manual.
The colors help find the values in MARTA P&ID screen 

<img alt="P & ID screen" src="doc/martaPID.png" height="200px">

Click on Address columns to monitor some values regularly each given intervall in seconds. 
The read values are written into an InfluxDB 2.0 database using connection information from the configuration file in: `connection.json`

Replace strings in uppercase by your own database connection information. ([installation of InfluxDB 2 on Linux](https://www.cyberithub.com/how-to-install-influxdb2-on-ubuntu-20-04-lts-step-by-step/))

When your InfluxDB dashboard is configured you can provide a direct link to it using the `dashboard` attribute

![influxDB 2 example](doc/influxTemps.png) 

## Alarms and Status

![Alarms](doc/martaAlarms.png)

## Set parameters

This page access is protected by a login / password that can be changed in the configuration file `data/users.json` using the following format : 

`"login" : "encrypted password"`

The encrypted password string can be obtained with the following command : 

`node -p 'cr=require("crypto");cr.createHmac("sha1","PASSWORD TO ENCRYPT").digest("hex")'`

Default value is login : `admin` and password : `marta`

<img alt="Parameters login" src="doc/martaParamLogin.png" height="200px"/>

![Parameters](doc/martaParameters.png)

