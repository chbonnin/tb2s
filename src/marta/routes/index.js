/* Copyright 2022 Institut Pluridisciplinaire Hubert Curien, CNRS, France
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

   FileName : 		index.js
   Content : 		MARTA device control using MODBUS protocol
   Used in : 		CMS
   Programmer : 	Christian Bonnin
   Version : 		1.0
   Date of creation 01/02/2022
   Support : 		mail to : christian.bonnin@iphc.cnrs.fr
*/
var express = require('express');
var router = express.Router();
const host = require('../connection.json')

/* GET home page. */
router.get('/', function(req, res, next) { 
  res.render('index', { 
    title: 'MARTA' , 
    connected: martaRouter.isConnected()?"Connected":"Disconnected",
    tooltip: martaRouter.isConnected()?"Click to disconnect":"Click to connect",
    urlDb: host.db.dashboard || host.db.url.replace(/\/[^:]*$/,"") //dashboard url if present OR DB url until port number
  });
});

router.get('/exit', function(req, res, next) {
  process.exit()
});

router.get('/connect', function(req, res, next) {
  if (!martaRouter.isConnected())
    martaRouter.connectDevice()
  else
    martaRouter.disconnectDevice(req, res)

  res.redirect("/")
});

router.get("/doc/:file", function(req, res){
  res.sendFile(req.params.file, {root:__dirname+"/../doc/"})
})

module.exports = router;
