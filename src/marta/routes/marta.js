/* Copyright 2022 Institut Pluridisciplinaire Hubert Curien, CNRS, France
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

   FileName : 		marta.js
   Content : 		MARTA device control using MODBUS protocol
   Used in : 		CMS
   Programmer : 	Christian Bonnin
   Version : 		1.0
   Date of creation 01/02/2022
   Support : 		mail to : christian.bonnin@iphc.cnrs.fr
*/
var express = require('express');
var modbus = require("modbus-serial");
const request = require("request")
const dateformat = require('dateformat');

var router = express.Router();
var client=new modbus()
const objVals= require('../data/readvalues.json')
const objAlarms= require('../data/alarms.json')
const host = require('../connection.json')
const arrLog=[]
var delayRefresh

router.isConnected=function(){
    return client.isOpen
}

router.connectDevice=function(){
    client.connectTCP(host.device.address, { port : host.device.port }) 
    client.setID(1) //MODBUS slave 1
}

router.disconnectDevice=function(req, res){
    client.close()
    const authToken = req.cookies['AuthToken'];
    delete global.loginRouter.authTokens[authToken] //logout
}

router.connectDevice()

/* GET read values */
router.get('/values', function(req, res, next) {
  //res.send('respond with Marta');
  const nbVal=56
  console.log("first value", objVals.values[0])
  client.readHoldingRegisters(100, nbVal*2, function(err, data) 
    {
        if (err){
            console.error(err)
            res.render('error', {title: "Error while reading MARTA values", error: err}) 
        } else {
            // console.log(data)
            buf=data.buffer.swap16()
            //buf=new Buffer(nbVal*4); buf.fill(0)            
            for (let iVal=0; iVal <buf.length; iVal+=4){
                objVals.values[iVal/4].Value=buf.slice(iVal, iVal+4).readFloatLE().toFixed(1)
                if (!objVals.values[iVal/4].Color)
                    objVals.values[iVal/4].Color="lightgray"
            }
            res.render('readValues',objVals)
        }
    })
});

/* GET alarms */
router.get('/alarms', function(req, res, next) {
    delayRefresh=req.query.delay>0 ? req.query.delay : ""
    // console.log("alarms delay", delayRefresh, new Date().toISOString())
    client.readHoldingRegisters(320, 1, function(err, data) 
    {
        if (err){
            console.error(err)
            res.render('error', {title: "Error while reading MARTA status", error: err}) 
        } else {
            statusVal=data.data[0]
            client.readHoldingRegisters(300, 4, function(err, data) 
            {
                if (err){
                    console.error(err)
                    res.render('error', {title: "Error while reading MARTA alarms", error: err}) 
                } else {
                    //console.log(data)
                    buf=data.buffer.swap16()
                    datNow=dateformat(new Date(), "yyyy-mm-dd, HH:MM:ss")
                    for (let iOct=0; iOct<data.buffer.length; iOct++){
                        for (let iBit=0; iBit<8; iBit++){
                            const valNew=(buf[iOct]>>iBit)&1
                            const objVal=iOct<4 ? objAlarms.chiller[iOct*8+iBit] : objAlarms.co2[(iOct-4)*8+iBit]
                            if (!("Value" in objVal) && valNew || "Value" in objVal && valNew!==objVal.Value)
                                arrLog.push({Date:datNow, Type:iOct<4 ? "Chiller":"CO2", Name:objVal.Description, State:valNew?"on":"off"})

                            objVal.Value=valNew
                        }
                    }
                    // console.log("delay ", delayRefresh)
                    res.render('alarmsStatus',{status: statusVal, alarms:objAlarms, log:arrLog, delayref:delayRefresh})
                }
            })
        }
    })
  });
  
router.get("/parameters", function(req, res, next){
  const authToken = req.cookies['AuthToken'];

  if (authToken && authToken in global.loginRouter.authTokens)
    client.readHoldingRegisters(204, 3*2, function(err, data) 
    {
        if (err){
            console.error(err)
            res.render('error', {title: "Error while reading MARTA parameters", error: err}) 
        } else {
            // console.log(data)
            buf=data.buffer.swap16()
            res.render("parameters", {  
                temperature: buf.slice(0, 4).readFloatLE().toFixed(1),
                speed: buf.slice(4, 8).readFloatLE().toFixed(0),
                flow: buf.slice(8, 12).readFloatLE().toFixed(1)
            })
        }
    })
  else
    res.render('login')
})

router.get("/paramset", function(req, res, next){
    console.log(req.query)
    buf=Buffer.alloc(12)
    buf.writeFloatLE(req.query.temp, 0)
    buf.writeFloatLE(req.query.speed, 4)
    buf.writeFloatLE(req.query.flow, 8)
    buf.swap16()

    arrVal=[]
    for (iOct=0; iOct<buf.length; iOct+=2){
        arrVal.push(buf[iOct]<<8 | buf[iOct+1])
    }
    console.log(arrVal)
    client.writeRegisters(310, buf, function(err, data){
        if (err){
            console.error(err)
            res.render('error', {title: "Error while read MARTA alarms", error: err}) 
        } else {
            res.redirect("/")
        }
    })
})

router.get("/paramactions", function(req, res, next){
    console.log(req.query)

    octAction = (req.query.chiller=="on"?1:0)
        |   (req.query.co2=="on"?1:0)<<1
        |   (req.query.flow=="on"?1:0)<<2
        |   (req.query.reset=="on"?1:0)<<3
    console.log(octAction)
    //res.render("parameters", {})
    client.writeRegisters(305, [octAction], function(err, data){
        if (err){
            console.error(err)
            res.render('error', {title: "Error while read MARTA alarms", error: err}) 
        } else {
            res.redirect("/")
        }
    })
})

router.get("/monitor/:addr", function(req, res){
    for (let iVal = 0; iVal<objVals.values.length ; iVal++){
        if (objVals.values[iVal].Address==req.params.addr) {
            res.render('monitorAsk', objVals.values[iVal]) 
            return
        }
    }
})

router.get("/monitorReq/:addr", function(req, res){
    // console.log("request", req.query)
    for (let iVal = 0; iVal<objVals.values.length ; iVal++){
        let objVal=objVals.values[iVal]
        if (objVal.Address==req.params.addr) {
            objVal.monitor=("yes" in req.query) ? req.query.period : 0
            // console.log("Interval", objVal.interval)
            if (objVal.interval){
                // console.log("clear",objVal.interval)
                clearInterval(objVal.interval)
                objVal.interval=0
            }
            if (parseInt( req.query.period )>0){
                objVal.interval = setInterval(sendValueToDb, parseInt( req.query.period ) * 1000, objVal)
                // console.log("setInterval", objVal.interval)
            }
            // sendValueToDb(objVal)
            res.redirect("/marta/values")
            return
        }
    }
})

function sendValueToDb(objVal){
    strData=objVal.Description.split(" ")[0] + " "
    switch (objVal.Unit){
        case "°C": case "°F": strData+="temperature";break;
        case "K": strData+="kelvin";break;
        case "%": strData+="percent";break;
        case "bara": strData+="pressure";break;
        case "rpm": strData+="speed";break;
        default: strData+="value";break;
    }
    client.readHoldingRegisters(objVal.Address, 2, function(err, data) 
    {
        if (err){
            console.error(err)
            res.render('error', {title: "Error while reading MARTA value at address "+objVal.Address, error: err}) 
        } else {
            // console.log(data)
            buf=data.buffer.swap16()
            objVal.Value=buf.slice(0, 4).readFloatLE().toFixed(1)
            objReq={
                url: host.db.url, 
                headers: host.db.headers, 
                body: host.db.measure+",sensor="+strData+"=" + objVal.Value
            }
            request.post(objReq, function(err, res, body){
                console.log("request data",strData, objVal.Value, "response=",res.statusCode, res.statusMessage)
            })
        }
    })

}

module.exports = router;
