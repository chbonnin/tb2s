// create an mock modbus server with values read from a text file
var fs = require('fs');
const FILENAME="data/values.dat"
var Modbus = require("modbus-serial");
let fd = fs.openSync(FILENAME, 'r+')
const buf = Buffer.alloc(2000)
let interval=0 

function rampSetTemp(){
        tempGoal=readFloatValue(204).value
        tempCurr=readFloatValue(156).value
        speed=readFloatValue(206).value
        console.log("goal=",tempGoal, "curr=",tempCurr, "speed=",speed)
        if (Math.floor(Math.abs(tempGoal-tempCurr))<speed){
            clearInterval(interval)
            interval=0
        } else{
            bufFloat=Buffer.alloc(4)
            bufFloat.writeFloatLE(tempCurr < tempGoal ? tempCurr + speed : tempCurr - speed, 0)
            bufFloat.swap16()
            writeFloatValue(261, bufFloat[0]<<8 | bufFloat[1])
            writeFloatValue(262, bufFloat[2]<<8 | bufFloat[3])
        }
}

function readFloatValue(addrBuf){
            if (addrBuf%2===0){
                fs.readSync(fd, buf, (addrBuf-1)*4, 4, (addrBuf-1)*5);
                addrFloat=addrBuf-1
            } else{
                addrFloat=addrBuf
                fs.readSync(fd, buf, (addrBuf+1)*4, 4, (addrBuf+1)*5);
            }
            strVal=String.fromCharCode(buf[addrFloat*4], buf[addrFloat*4+1], buf[addrFloat*4+2], buf[addrFloat*4+3])+"."+
                    String.fromCharCode(buf[addrFloat*4+4], buf[addrFloat*4+5], buf[addrFloat*4+6], buf[addrFloat*4+7])

            val=parseFloat(strVal)
            bufFloat=Buffer.alloc(4)
            bufFloat.writeFloatLE(val, 0)
            bufFloat.swap16()
            return {bytes: addrBuf%2===0 ?  bufFloat[2]<<8 | bufFloat[3] : bufFloat[0]<<8 | bufFloat[1]
                    , value: val, string: strVal}
}

function writeFloatValue(addrBuf, value){
        strVal=("000"+value.toString(16)).slice(-4)
        if (addrBuf%2===1){// first 2 bytes (4 hexa digits) are stored
            // console.log("@",addrBuf,"value=",value)
            for (let iDgt=0; iDgt<4; iDgt++)
                buf[addrBuf*4+iDgt]=strVal.charCodeAt(iDgt)
        } else {
            bufFloat=Buffer.alloc(4)
            bufFloat[0]=parseInt(String.fromCharCode(buf[addrBuf*4-4], buf[addrBuf*4-3]),16)
            bufFloat[1]=parseInt(String.fromCharCode(buf[addrBuf*4-2], buf[addrBuf*4-1]),16)
            bufFloat[2]=value>>8
            bufFloat[3]=value&255
            bufFloat.swap16()
            val=bufFloat.readFloatLE() // float value
            strInt=("000"+Math.floor(val)).slice(-4)
            strDec=val.toFixed(4).slice(-4)
            addrWrite=addrBuf - 107 // values are read at another address
            console.log("float=", val, strInt+","+strDec,"@",addrBuf, "=>", addrWrite+1)
            fs.writeSync(fd, strInt+"\n"+strDec, addrWrite*5 , 9, 0) // args NOT IN SAME ORDER as in readSync ???!
        }
}

var vector = {
    getInputRegister: function(addr, unitID) {
        console.log(Date().toString().split(" ")[4],"getInputRegister", addr, unitID)
        // Synchronous handling
        return addr;
    },
    getHoldingRegister: function(addr, unitID, callback) {//addr = line number in data file
        addrBuf=addr-1 //address in data buffer
        let strVal, val, addrFloat;
        fs.readSync(fd, buf, addrBuf*4, 4, addrBuf*5);
        if (addr>=300 && addr<=305 || addr===320){//Hexadecimal values
            strVal=String.fromCharCode(buf[addrBuf*4], buf[addrBuf*4+1], buf[addrBuf*4+2], buf[addrBuf*4+3])
            val=parseInt(strVal, 16)
        } else {//Float values : first line is integer part and second line is decimal part 
            objVal=readFloatValue(addrBuf)
            strVal=objVal.string
            val=objVal.bytes
        }
        console.log(Date().toString().split(" ")[4],"getHoldingRegister @", addr,':', strVal)
        // Asynchronous handling (with callback)
        setTimeout(function() {
            // callback = function(err, value)
            callback(null, val);
        }, 10);
    },
    getCoil: function(addr, unitID) {
        console.log(Date().toString().split(" ")[4],"getCoil", addr, unitID)
        // Asynchronous handling (with Promises, async/await supported)
        return new Promise(function(resolve) {
            setTimeout(function() {
                resolve((addr % 2) === 0);
            }, 10);
        });
    },
    setRegister: function(addr, value, unitID) {
        // Asynchronous handling supported also here
        strVal=("000"+value.toString(16)).slice(-4)
        console.log(Date().toString().split(" ")[4],"set register", addr, "0x"+strVal, unitID);
        if (addr==305 && (value&1)){//start chiller
            clearInterval(interval)
            interval=setInterval(rampSetTemp,1000)
        }
        if (addr<309) return // actions are not written into text file
        addrBuf=addr-1 //address in data buffer
        writeFloatValue(addrBuf, value)
    },
    setCoil: function(addr, value, unitID) {
        // Asynchronous handling supported also here
        console.log(Date().toString().split(" ")[4],"set coil", addr, value, unitID);
        return;
    },
    readDeviceIdentification: function(addr) {
        console.log(Date().toString().split(" ")[4],"readDeviceIdentification", addr)
        return {
            0x00: "MyVendorName",
            0x01: "MyProductCode",
            0x02: "MyMajorMinorRevision",
            0x05: "MyModelName",
            0x97: "MyExtendedObject1",
            0xAB: "MyExtendedObject2"
        };
    }
};

// set the server to answer for modbus requests
console.log("ModbusTCP listening on modbus://0.0.0.0:8502 and reading data from: "+FILENAME);
var serverTCP = new Modbus.ServerTCP(vector, { host: "0.0.0.0", port: 8502, debug: true, unitID: 1 });

serverTCP.on("socketError", function(err){
    // Handle socket error if needed, can be ignored
    console.error(err);
});
