#include <OneWire.h>
#include <DallasTemperature.h>
#include <Wire.h>
#include "SHTSensor.h"

#include <ESP8266WiFi.h>
extern "C" {
#include "user_interface.h"
#include "wpa2_enterprise.h"
}

/***** declare global wifi variables to hold the credentials for the network  */

const char* ssid = "IPHC";
const char* identity = "cbonnin";
const char* username = "cbonnin";
const char* password = "ldap";


/****** Data wire is plugged into pin 4 on the Arduino **/
#define ONE_WIRE_BUS 4


/****** Software I2C pins used -hence SDA and SCL **/
#define SDA_PIN 13
#define SCL_PIN 14

/****** declare the objects here; **/
// Setup a oneWire instance to communicate with any OneWire devices
// (not just Maxim/Dallas temperature ICs)
OneWire oneWire(ONE_WIRE_BUS);

// Pass our oneWire reference to Dallas Temperature.
DallasTemperature ds18b20(&oneWire);


SHTSensor sht85(SHTSensor::SHT3X);


long then = 0;

void setup()
{
  //WiFi.mode(WIFI_STA);

  // start serial port
  Serial.begin(115200);

  // start WiFi
  delay(10);
  connectWifiWpa2();

  // Start up the library
  ds18b20.begin();

  Wire.begin(SDA_PIN, SCL_PIN);
  sht85.init();

  if (! sht85.init()) // check the state of the sensor
  {
    Serial.println("Couldn't find SHT85");

    Wire.begin(SDA_PIN, SCL_PIN);

    //while (1) delay(1);
  }
  else {
    Serial.println("Found SHT85");
  }

}
void connectWifiWpa2(){
  //WiFi.disconnect(true);      
  wifi_set_opmode(STATION_MODE);
  struct station_config wifi_config;
  memset(&wifi_config, 0, sizeof(wifi_config));
  strcpy((char*)wifi_config.ssid, ssid);
  wifi_station_set_config(&wifi_config);
  
  wifi_station_set_wpa2_enterprise_auth(1);
  wifi_station_clear_cert_key();
  wifi_station_clear_enterprise_ca_cert();
  wifi_station_clear_enterprise_identity();
  wifi_station_clear_enterprise_username();
  wifi_station_clear_enterprise_password();
  wifi_station_clear_enterprise_new_password();
  wifi_station_set_enterprise_identity((uint8*)identity, strlen(identity));
  wifi_station_set_enterprise_username((uint8*)username, strlen(username));
  wifi_station_set_enterprise_password((uint8*)password, strlen(password));
  wifi_station_connect();
    
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print("-");
  }
  Serial.println("");
  Serial.print("WiFi connected on IP address ");
  Serial.println(WiFi.localIP());
  Serial.print("MAC address: ");
  Serial.println(WiFi.macAddress());
}
/*
void connectWifi() {
  delay(250);
  // Connecting to a WiFi network
  Serial.print("\nConnecting to:");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print("*");
  }
  Serial.println("");
  Serial.print("WiFi connected on IP address ");
  Serial.println(WiFi.localIP());
  Serial.print("MAC address: ");
  Serial.println(WiFi.macAddress());
}
*/
double dewpoint(float t, float h)
{
  float tn = t < 0.0 ? 272.62 : 243.12;
  float m = t < 0.0 ? 22.46 : 17.62;

  float l = logf(h / 100.0);
  float r = m * t / (tn + t);

  return tn * (l + r) / (m - l - r);
}




void loop()
{
  long now = millis();  //insert a sampling frequency of 5 secs
  if (now - then < 5000) return;

  then = now;

  //bool statusds 
  ds18b20.requestTemperatures();
  float temp = ds18b20.getTempCByIndex(0);

  //bool statussht = sht85.readSample();
  float tempsht = sht85.getTemperature();
  float hum = sht85.getHumidity();
  float dp = dewpoint(tempsht, hum);

  if (! isnan(temp))
  {

    Serial.print("TempDS = ");
    Serial.println(temp);
  }
  else
  {
    Serial.println("Failed to read temperature");
  }



  if (! isnan(tempsht))
  {
    //Serial.print("Requesting temperature from SHT85");
    Serial.print("TempSHT = ");
    Serial.println(tempsht);
    // delay(100);
  }
  else
  {
    Serial.println("Failed to read temperature");
  }

  if (! isnan(hum))
  {
    //Serial.print("Requesting humidity from SHT85");
    Serial.print("Hum = ");
    Serial.println(hum);
    //  delay(100);
  }
  else
  {
    Serial.println("Failed to read humidity");
  }

  if (! isnan(dp))
  {
    //Serial.print("Requesting humidity from SHT85");
    Serial.print("Dewpoint = ");
    Serial.println(dp);
    //  delay(100);
  }
  else
  {
    Serial.println("Failed to read dewpoint");

  } 
}
