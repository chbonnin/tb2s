# requests to DB

## In development database
Tables list

`python3 rhapi.py -u https://cmsdca.cern.ch/trk_rhapi --login trker_int2r`

Table content

`python3 rhapi.py -u https://cmsdca.cern.ch/trk_rhapi --login trker_int2r.p17820`

Request 2S module data from ID

`python3 rhapi.py -u https://cmsdca.cern.ch/trk_rhapi --login "select * from trker_int2r.p10620 p where p.id=1776877"`

Same request with PARTS table

`python3 rhapi.py -u https://cmsdca.cern.ch/trk_rhapi --login "select * from trker_int2r.parts p where p.id=1776877"`

Parent of this module

 `python3 rhapi.py -u https://cmsdca.cern.ch/trk_rhapi --login "select * from trker_int2r.parts p where p.id=1774450"`

Request for Ladder parts with join

`python3 rhapi.py --login -u https://cmsdca.cern.ch/trk_rhapi "select p.*, k.BARCODE as PART_PARENT_BARCODE, k.SERIAL_NUMBER as PART_PARENT_SERIAL, k.KIND_OF_PART as PART_PARENT_KOP from trker_int2r.p17820 p left join trker_int2r.parts k on p.PART_PARENT_ID = k.ID left join trker_int2r.parts d on p.ID = d.ID left join trker_int2r.trkr_locations_v l on d.LOCATION_ID = l.LOCATION_ID where l.LOCATION_NAME = 'CERN'"`

Insertion/modification of data in dev database
 `python3 cmsdbldr_client.py --login -u https://cmsdca.cern.ch/trk_loader/trker/int2r data/ladder.xml`