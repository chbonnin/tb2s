from django.http import HttpResponse
from django.shortcuts import render
import django.template
import mimetypes
import ilock
from . import rhapi

arrTables=[]
arrFolders=[]
strTable=""
strFolder=""
pageSize=100
pageNum=1

def index(request):
    print(request)
    template=django.template.Template('<table><tr>'
        +'<td><form action="tables">Choose a base<br>'
        +'<select name="folder" onchange="this.form.submit()" size="2">{% for elem in folders %} <option>{{elem}}</option> {%endfor%}</select>'
        +'<br><input type="submit"></form></td><td></td></table>')
    context=django.template.Context({'folders':getFolderNames()})
    return HttpResponse(template.render(context))

def exportCsv(arrDat, setCols):
    strData=""
    for strCol in setCols:
        if strData: strData+=","
        strData+=strCol

    strData+="\n"
    for arrLine in arrDat:
        strLine=""
        for strVal in arrLine:
            if strLine: strLine +=","
            strLine+=str(strVal)

        strData+=strLine+"\n"

    response = HttpResponse(strData)
    response['Content-Type'] = 'text/csv'
    response['Content-Disposition'] = 'attachment; filename=data.csv'
    return response

def tableContent(request):
    global strTable, pageSize, pageNum
#    strFolder=request.GET['folder']
    strTable=request.GET['table']
    pageSize=int(request.GET['size'])
    pageNum=int(request.GET['page'])
    read=rhapi.RhApi("https://cmsdca.cern.ch/trk_rhapi", sso="login")
    if pageNum>0:
        data=read.json2("select * from %s.%s t"%(strFolder, strTable), pagesize=pageSize, page=pageNum)# ou json ou json2 ou csv
        setCols=set()
        for dat in data['data']:
            setCols |= set(dat)
        
        arrDat=[]
        for dat in data['data']:
            arrLine=[]
            for col in setCols:
                arrLine.append(dat[col] if col in dat else "")
            
            arrDat.append(arrLine)

#        print(strFolder, strTable, arrFolders)  
    else: #if pageNum<=0, display information about table : fields, and number of lines
        setCols=["Column Name","Type"]
        arrFld=read.table(strFolder, strTable)
        arrDat=[]
        for field in arrFld['columns']:
            arrDat.append([field["name"], field["type"]])

        arrDat.append(["",""]) #empty line
        arrDat.append(["Lines count", read.count(read.qid("select * from %s.%s t"%(strFolder, strTable)))])

    if 'export' in request.GET:
        return exportCsv(arrDat, setCols)
    else:
        contextDict={'folders':arrFolders, 'tables':arrTables, 'folder':strFolder, 
                    'table':strTable, 'cols':setCols, 'data':arrDat, 'size':pageSize, 'page':pageNum}
        return render(request, template_name="tableAndContent.html", context=contextDict)

def tableList(request):
    global strTable, strFolder, arrTables
    strFolder=request.GET['folder']
    strTable=""
    read=rhapi.RhApi("https://cmsdca.cern.ch/trk_rhapi", sso="login")
    arrTables=read.tables(strFolder)
    #print(strFolder, strTable, arrFolders)  
    arrTables.sort(key=lambda x: '|'+x if x[1:].isdigit() else x) # condition et part additional tables at the end
    contextDict={'tables':arrTables, 'folders':arrFolders, 'folder': strFolder, 'size':pageSize, 'page':pageNum}
    return render(request, template_name="tableAndContent.html", context=contextDict)
    
def getFolderNames():
    global arrFolders
    if len(arrFolders)==0:
        read=rhapi.RhApi("https://cmsdca.cern.ch/trk_rhapi", sso="login")
        arrFolders=read.folders()

    return arrFolders
