# MetaTracker Bluetooth sensors
## Monitoring (streaming)
### Dans un navigateur
Dans le répertoire
`cd ~/devpt/tb2s/src/mbientlab`

Lancer
`./serverMeta.py`

Dans un navigateur, aller sur 
[http://localhost:8000/metatracker/main](http://localhost:8000/metatracker/main)

### En ligne de commande
Vérifier que la variable d'environnement INFLUXDB_TOKEN existe bien avec

`echo $INFLUXDB_TOKEN`

sinon, lancer 
`source ~/devpt/tb2s/influxdb/data/setup.sh`

Aller dans le répertoire
`cd ~/devpt/tb2s/src/mbientlab`

`./stream.py <sensor name> -GAPHT [-d durée en s]`

Les capteurs ont pour nom mt1, mt2, mt3, mt4.
Les options en majuscule sont les types de données mesurées (Gyroscope, Accéléromètre, Pression, Humidité, Température)
Lancer le script sans argument pour connaitre la liste des options

## Visualisation des données
Dans un navigateur, aller sur 
[http://localhost:8086](http://localhost:8086)

login : integration (integration)

Aller dans Boards, cliquer sur le Dashboard Metatracker

## Acquisition (log)
Sur un mobile, l'application MetaBase permet de lancer une session d'acquisition. Les fichiers doivent ensuite être transférés sur PC
Copier ces fichiers dans le conteneur Docker de la base de données InfluxDB avec 

`docker cp <répertoire contenant les fichiers> tb2s:/home/influxdb/data/`

Entrer dans un terminal sur ce conteneur avec 
`docker exec -it tb2s bash`

Aller dans le répertoire contenant les données
`cd /home/influxdb/data`

Utiliser le script bash pour importer les données
`./importData.sh <sensor name> <fichier(s)>`
## Applications
Sur iPhone, une application supplémentaire nommée MetaWear est disponible et permet d'accéder à plus de fonctionnalités
