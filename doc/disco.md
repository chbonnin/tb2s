:::mermaid
flowchart LR
    A[DISCO] -->|WiFi| B(MQTT server)
    subgraph     
    direction TB
      L(AutoconnectAP) -. 192.168.4.1 .-> A
      style L stroke-dasharray: 5 5
    end
    B -- firewall ! --- B
    B --> |mosquitto_sub| T[Telegraf plugin]
    subgraph DockerT [Docker]
    T
    end
    T --data --> E[(InfluxDB 2)]
    subgraph DockerI [Docker]
    E
    end
:::